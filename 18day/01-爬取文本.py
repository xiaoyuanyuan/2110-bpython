"""
    数据采集(爬虫)：写一段程序，通过一定规则，把数据采集下来。
    用的语言：Java和Python
    爬虫分类：聚焦爬虫和通用爬虫
    爬虫违法：处于灰色地带,遵循一些法则
    爬虫协议：robots.txt
    爬虫库：requests(网络请求库)、scrapy(分布式爬虫框架)
    爬虫原理：通过模拟发送HTTP请求，获取数据
    反爬虫：解决方案，反反爬虫 解决方法：换IP，逆向，动态手段
    安装库：pip install requests -i 镜像源
    请求方式；Get 和Post

    请求：request
    响应：response
"""

"""
    1、定位目标数据源
    2、发起网络请求
    3、清洗数据
    4、保存数据
"""
import requests
import re
import csv
response = requests.get("http://xiaodiaodaya.cn/xh/fq/")
# 自动解码
# print(response.text)
# 手动解码
html = response.content.decode("utf8")
# with open("xh.html", "w", encoding="utf8") as f:
#     f.write(html)
# 清洗数据：re、xpath、bs4
content = re.findall('<a.*?>(.*?)</a>', html, re.S)
content = content[:len(content) - 1]
# content = [content[:len(content) - 1]]
# # 保存csv
# with open('output.csv', mode='w', newline='', encoding="utf8") as file:
#     writer = csv.writer(file)  # 创建 CSV 写入器对象
#     writer.writerows(content)  # 将数据写入到 CSV 文件

for c in content:
    with open("output.csv", "a", encoding="utf-8") as f:
        f.write(c + "\n")
