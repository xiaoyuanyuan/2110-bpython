import requests
import re
import pandas as pd
import csv
import os


class LianJiaSpider():
    def __init__(self):
        # 数据源
        self.url = "https://bj.lianjia.com/ershoufang/pg{}/"
        # 写入操作（一次写一行）(列表写入)
        if not os.path.exists("data.csv"):
            with open('data.csv', 'w', encoding='utf-8-sig') as csvfile:  # utf-8-sig让生成的csv文件带有DOM
                writer = csv.writer(csvfile)  # 生成一个“写手”
                writer.writerow(['房间标题', '房间类型', '房间平米', '房间朝向', '装修形式', '房间楼层', "建设年限", "楼型", "关注人数", "发布日期"])  # 一次写一

    # 发送请求
    def send_request(self, url):
        print("正在请求{}".format(url))
        response = requests.get(url)
        return response

    # 解析数据
    def parse_content(self, response):
        content = response.text
        # with open("lj.html", "w", encoding="utf8") as f:
        #     f.write(content)
        data = re.findall(
            '<div.*?class="title".*?data-sl="">(.*?)<.*?<div.*?class="houseInfo"><span.*?</span>(.*?)</div>.*?<div.*?class="followInfo"><span.*?</span>(.*?)</div>',
            content, re.S)
        self.save_data(data)

    # 保存数据
    def save_data(self, data):
        print("正在保存数据")
        for d in data:
            title = d[0]
            home_info = d[1].split("|")
            if len(home_info) == 6:
                home_info.insert(5, 1970)
            follow, date = d[2].split("/")
            l = [title, home_info[0], home_info[1], home_info[2], home_info[3], home_info[4], home_info[5],
                 home_info[6], follow, date]
            with open('data.csv', 'a', encoding='utf-8-sig') as csvfile:  # utf-8-sig让生成的csv文件带有DOM
                writer = csv.writer(csvfile)  # 生成一个“写手”
                writer.writerow(l)

    # 开始爬虫
    def start(self):
        # 200 301 302 404 405 500 502
        for i in range(1, 11):
            url = self.url.format(i)
            response = self.send_request(url)
            if response and response.status_code == 200:
                self.parse_content(response)


if __name__ == '__main__':
    ljs = LianJiaSpider()
    ljs.start()
