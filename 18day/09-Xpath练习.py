from lxml import etree

text = '''
<div>
    <ul>
        <li class="one"><a href="link1">1</a></li>
        <li class="two"><a href="link2">2</a></li>
        <li class="three"><a href="link3">3</a></li>
        <li class="four"><a href="link4">4</a></li>
        <li class="five"><a href="link5">5</a>
    </ul>
</div>
'''
# 将文本转换为网页类型，并修复补全
html = etree.HTML(text)
# 选择内容匹配
# // 任意节点
# result = html.xpath('//li/a')
# print(result)
# for res in result:
#     # print(res.text)  # 取标签的值
#     # . 当前目录  返回列表
#     print(res.xpath("./text()"))  # 取标签的值
#     print(res.xpath("./@href"))  # 取标签的值

# three = html.xpath('//li[@class="three"]//a//text()')
# print(three)
a = html.xpath('//a[@href="link3"]//text()')
print(a)


"""
https://video.pearvideo.com/mp4/short/20240508/cont-1794080-71106734-hd.mp4
https://image.pearvideo.com/cont/20240508/cont-1794080-12760938.jpg
"""