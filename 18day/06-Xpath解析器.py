"""
    Xpath
    pip install lxml -i 镜像源
"""
import random
import time
import re
import os
import requests
from lxml import etree


class PearVideoSpider():
    def __init__(self):
        self.url = "https://www.pearvideo.com/category_1"
        # 反爬加UA  UA标识浏览器
        self.headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36",

        }

        self.referer = "https://www.pearvideo.com/video_{}"
        self.ajax_url = "https://www.pearvideo.com/videoStatus.jsp?contId={}&mrd={}"
        if not os.path.exists("video"):
            os.mkdir("video")

    def send_request(self, url):
        print("正在请求{}".format(url))
        response = requests.get(url, headers=self.headers)
        return response

    def parse_content(self, response):
        content = response.text
        with open("li.html", "w", encoding="utf8") as f:
            f.write(content)
        html = etree.HTML(content)
        hrefs = html.xpath('//a[contains(@href,"video_")]/@href')
        for href in hrefs:
            #
            # full_url = "https://www.pearvideo.com/" + href
            # 发起请求
            full_ajax_url = self.ajax_url.format(href.split("_")[1], random.random())
            full_referer = self.referer.format(href.split("_")[1])
            # 加入到请求头
            self.headers["Referer"] = full_referer
            time.sleep(2)
            response = self.send_request(full_ajax_url)
            self.parse_video_content(response, href.split("_")[1])

    def parse_video_content(self, response, video_id):

        # 提取出视频mp4地址
        content_json = response.json()
        srcUrl = content_json["videoInfo"]["videos"]["srcUrl"]
        split_list = srcUrl.split("-")
        base_video_url = split_list[0].rsplit("/", 1)[0]
        # 最终的视频URL
        video_url = base_video_url + "/cont-" + video_id + "-" + split_list[1] + "-" + split_list[2]
        print(video_url)

        # 重置视频地址
        "https://video.pearvideo.com/mp4/adshort/20180325/1715412070444-11748175_adpkg-ad_hd.mp4"
        "https://video.pearvideo.com/mp4/adshort/20200621/cont-{}-15216618_adpkg-ad_hd.mp4"
        "https://video.pearvideo.com/mp4/adshort/20200621/cont-1681404-15216618_adpkg-ad_hd.mp4"
        # 发送视频地址
        self.headers["Referer"] = "https://www.pearvideo.com/"
        response = self.send_request(video_url)
        self.save_data(response, video_url)

    def save_data(self, response, video_url):
        video_name = video_url.rsplit("/", 1)[-1]
        print("正在保存{}".format(video_name))
        with open("video/" + video_name, "wb") as f:
            f.write(response.content)

    def start(self):
        response = self.send_request(self.url)
        if response and response.status_code == 200:
            self.parse_content(response)


pvs = PearVideoSpider()
pvs.start()

"""
https://video.pearvideo.com/mp4/adshort/20200621/1715411740150-15216618_adpkg-ad_hd.mp4
https://video.pearvideo.com/mp4/adshort/20200621/cont-1681404-15216618_adpkg-ad_hd.mp4

"""
