import requests
import re
import os

if not os.path.exists("pic"):
    os.mkdir("pic")
# 发起请求
response = requests.get("https://www.169tp.com/gaoxiaotupian/")
content = response.content.decode("gb2312")
# with open("tp.html", "w", encoding="utf8") as f:
#     f.write(content)
url_list = re.findall("https://img.1000tuku.com:8899/uploads/allimg/.*?.jpg", content, re.S)
# 发起图片请求
for url in url_list:
    filename = url[-1:-15:-1][::-1]
    response = requests.get(url)
    with open("pic/" + filename, "wb") as f:
        f.write(response.content)
