import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# 图形绘制
# fig = plt.figure(figsize=(8, 5))
#
# x = np.linspace(0, 2 * np.pi)
# plt.plot(x, np.sin(x))  # 正弦曲线
# plt.plot(x, np.cos(x))  # 余弦曲线

# 图例
# 'best', 'upper right', 'upper left', 'lower left', 'lower right', 'right', 'center left', 'center right', 'lower center', 'upper center', 'center'
# plt.legend(["Sin", "Cos"], fontsize=16, loc="lower left", ncol=1)
# plt.show()


#  线条属性

fig = plt.figure(figsize=(8, 5))
x = np.linspace(0, 2 * np.pi, 20)
y1 = np.sin(x)
y2 = np.cos(x)

"""
# c : color 线颜色
# marker: 标记的样式或点的样式
# mfc: marker face color 标记的背景颜色
# ls : line style 线的样式
# lw: line width 线的宽度
# label: 线标签（图例中显示）
"""

plt.plot(x, y1, c='r', marker="o", ls="--", lw=1, label="sinx", mfc="y")
plt.plot(x, y2, c='b', marker='*', ls='-', lw=2, label='cosx', mfc='g' )
plt.plot(x, y1-y2, c='y', marker='^', ls='-', lw=3, label='sinx-cosx', mfc='b', alpha=1)

plt.plot(x, y1+y2, c='orange', marker='>', ls='-.', lw=4, label='sinx+cosx',
         mfc='y',
         markersize=10,  # 点大小
         markeredgecolor='green',  #点边缘颜色
         markeredgewidth=2  # 点边缘宽度
        )
plt.legend()
plt.show()
