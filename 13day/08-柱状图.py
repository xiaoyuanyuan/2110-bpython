import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# 让图片中可以显示中文
plt.rcParams['font.sans-serif'] = 'SimHei'
# 让图片中可以显示负号
plt.rcParams['axes.unicode_minus'] = False
fig = plt.figure(figsize=(8, 5))
# x = ['语文', '数学', '英语', 'Python', '化学']
# y = [20, 10, 40, 60, 10]
# plt.bar(x, y)

# df = pd.read_excel("../data/plot.xlsx", sheet_name="bar1")
# x, y = df["年份"], df["销售额"]
# plt.bar(x, y,width=0.6,color="r")
# for a, b in zip(x, y):
#     plt.text(x=a, y=b + 5e4, s="{:.1f}万".format(b / 10000), ha="center")


# 簇状柱形图
# df = pd.read_excel("../data/plot.xlsx", sheet_name="bar2")
# x, y1, y2, y3 = df.年份, df.北区, df.中区, df.南区
# w = 0.2
# plt.bar(x - w, y1, width=w, label="北区")
# plt.bar(x, y2, width=w, label="中区")
# plt.bar(x + w, y3, width=w, label="南区")


# 堆叠柱形图
df2 = pd.read_excel('../data/plot.xlsx', sheet_name='bar2')
x, y1, y2, y3 = df2.年份, df2.北区, df2.中区, df2.南区

# plt.bar(x, y1, label="北区")
# plt.bar(x, y2, label="中区", bottom=y1)
# plt.bar(x, y3, label="南区", bottom=y1 + y2)


# 条形图
plt.barh(x, y1)

plt.xlabel("年份")
plt.ylabel("销售额")
plt.title("2014-2018销售额统计")
plt.legend()
# 自动调整布局
fig.tight_layout()
plt.show()
