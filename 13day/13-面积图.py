import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# 让图片中可以显示中文
plt.rcParams['font.sans-serif'] = 'SimHei'
# 让图片中可以显示负号
plt.rcParams['axes.unicode_minus'] = False
fig = plt.figure(figsize=(8, 5))


# x = [1, 2, 3, 4, 5]
# y = np.random.randint(10, 100, 5)
#
# # 面积图
# plt.stackplot(x, y)


df = pd.read_excel('../data/plot.xlsx', sheet_name='stackplot1')
x, y = df['年份'], df['销售额']

plt.figure(dpi=100)
plt.stackplot(x, y)
# plt.plot(x, y)

plt.show()