import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# x = np.linspace(0, 10)
# y = np.sin(x)
# plt.plot(x, y)
#
# plt.xticks(np.arange(0, 11, 1),color="r")
# plt.yticks(ticks=[-1, 0, 1], labels=["min", 0, "max"], fontsize=20, ha="right",c="blue")
# plt.show()


# x = np.linspace(0, 2 * np.pi)
# y = np.sin(x)
# plt.plot(x, y, c='r')
# # 设置x轴范围
# plt.xlim(-2, 8)
# plt.xticks(np.arange(-2, 9, 1), color="r")
# # # 设置 y轴范围
# plt.ylim(-2, 2)
# plt.show()


# sin曲线
# x = np.linspace(0, 2*np.pi)
# y = np.sin(x)
# plt.plot(x, y, c='r')
# # 坐标轴范围：[xmin, xmax, ymin, ymax]
# plt.axis([-2, 8, -2, 2])

# 选项
# off ： 不显示坐标轴
# equal: 让x轴和y轴 刻度距离相等
# scaled：自动缩放坐标轴和图片适配
# tight：紧凑型自动适配图片
# square：x轴和y轴宽高相同
# plt.axis('square')
# plt.show()


fig = plt.figure(figsize=(8, 5))

# 让图片中可以显示中文
plt.rcParams['font.sans-serif'] = 'SimHei'
# 让图片中可以显示负号
plt.rcParams['axes.unicode_minus'] = False

x = np.linspace(0, 10)
y = np.sin(x)
plt.plot(x, y)
plt.title("sin曲线", fontsize=20, loc="center")
# todo 参数位置
# plt.suptitle("父标题", fontsize=22)
# 网格线
plt.grid(ls="--", lw="0.5", c="gray", axis="x")
plt.show()
