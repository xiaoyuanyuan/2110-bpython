import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# 让图片中可以显示中文
plt.rcParams['font.sans-serif'] = 'SimHei'
# 让图片中可以显示负号
plt.rcParams['axes.unicode_minus'] = False
fig = plt.figure(figsize=(8, 5))

# x = [10, 20, 30, 40]
#
# plt.pie(x, autopct='%.2f%%')
# plt.show()

df = pd.read_excel("../data/plot.xlsx", sheet_name="pie1")
c, v = df.省份, df.销量
# plt.pie(x=v, autopct="%.1f%%", labels=c, pctdistance=0.8, explode=[0, 0, 0, 0.1, 0, 0, 0, 0, 0, 0],
#         textprops={"fontsize": 12, "color": "blue"}, shadow=True)

# 饼图
plt.pie(
    x=v,  # 值
    autopct='%.1f%%',  # 百分比
    labels=c,  # 标签
    pctdistance=0.8,  # 百分比文字的位置
    # 字体样式
    textprops={'fontsize': 10, 'color': 'k'},
    # 甜甜圈设置
    wedgeprops={'width': 0.4, 'edgecolor': 'w'}
)

plt.pie(
            x=v,   # 值
            autopct='%.1f%%',  # 百分比
           #  labels=citys2,  # 标签
            pctdistance=0.8,  # 百分比文字的位置
            # 字体样式
            textprops={'fontsize': 8, 'color': 'k'},
            # 半径
            radius=0.3
       )
plt.show()
