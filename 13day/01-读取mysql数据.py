from sqlalchemy import create_engine
import pandas as pd
import numpy as np
from pandas import Series, DataFrame

data = np.random.randint(0, 150, size=(150, 3))
df = pd.DataFrame(data=data, columns=['Python', 'Pandas', 'PyTorch'])

conn = create_engine("mysql+pymysql://root:123456@localhost/db_16_04")

df.to_sql("score", conn, index=False, if_exists="append")


# df1 = pd.read_sql(sql='select * from score', con=conn)
# print(df1)
