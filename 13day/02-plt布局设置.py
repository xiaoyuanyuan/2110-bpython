import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# 让图片中可以显示中文
plt.rcParams['font.sans-serif'] = 'SimHei'
# 让图片中可以显示负号
plt.rcParams['axes.unicode_minus'] = False

# x = np.linspace(-5, 5, 50)
# y = x ** 2
# # 画线形图
# plt.plot(x, y)
# plt.plot(x, y, "g:")
# plt.show()

# 画布
fig = plt.figure(figsize=(5, 3), dpi=100, facecolor='#11aa11')
# x = np.linspace(0, 2 * np.pi)
# y = np.sin(x)
# plt.plot(x, y)
# plt.show()


# 显示多个图
# x = np.linspace(0, 8)
# plt.plot(x, np.sin(x))
# plt.plot(x, np.cos(x), 'r')
# plt.plot(x, -np.sin(x), 'g--')
# plt.show()


# 多图布局
fig = plt.figure(figsize=(8, 5))
x = np.linspace(-np.pi, np.pi, 30)
y = np.sin(x)
a1 = plt.subplot(221)  # 2行2列的第1个图
a1.set_title("a1")
a1.plot(x, y)

a2 = plt.subplot(222)  # 2行2列的第2个图
a2.set_title("a2")
a2.plot(x, y)

a3 = plt.subplot(223)  # 2行2列的第3个图
a3.set_title("a3")
a3.plot(x, y)

a4 = plt.subplot(224)  # 2行2列的第4个图
a4.set_title("a4")
a4.plot(x, y)

# 自动调整布局
fig.tight_layout()
plt.show()


