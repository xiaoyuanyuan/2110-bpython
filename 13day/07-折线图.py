import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# 让图片中可以显示中文
plt.rcParams['font.sans-serif'] = 'SimHei'
# 让图片中可以显示负号
plt.rcParams['axes.unicode_minus'] = False
fig = plt.figure(figsize=(8, 5))

# x = ["Mon", "Tues", "Wed", "Thur", "Fri", "Sat", "Sun"]
# y = [20, 40, 35, 55, 42, 80, 50]
#
# plt.ylim(0, 100)
# plt.plot(x, y, marker="o")
#
# plt.xlabel("星期")
# plt.ylabel("活跃度")
# plt.title("Python语言活跃度")
#
# for a, b in zip(x, y):
#     plt.text(a, b + 2, s=b, ha="center", va="bottom")


# x = np.random.randint(0, 10, size=15)
# plt.plot(x, marker='*', color='r')
# plt.plot(x.cumsum(), marker='o')


df = pd.read_excel("../data/plot.xlsx", sheet_name="line")

x, y1, y2, y3 = df.月份, df.语文, df.数学, df.英语

plt.plot(x, y1, label="语文", marker="*")
plt.plot(x, y2, label="数学", marker="o")
plt.plot(x, y3, label="英语", marker="+")

plt.xlabel("月份")
plt.ylabel("成绩")
plt.title("成绩的变化趋势")
plt.legend()
# plt.grid()
# 自动调整布局
fig.tight_layout()
plt.show()
