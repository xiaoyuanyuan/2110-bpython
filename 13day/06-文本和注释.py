import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# 让图片中可以显示中文
plt.rcParams['font.sans-serif'] = 'SimHei'
# 让图片中可以显示负号
plt.rcParams['axes.unicode_minus'] = False
fig = plt.figure(figsize=(5, 3))

x = np.linspace(0, 10, 10)
y = np.array([60, 30, 20, 90, 40, 60, 50, 80, 70, 30])

plt.plot(x, y, ls='--', marker='o')



# 文本
for a, b in zip(x, y):
    plt.text(x=a + 0.3, y=b + 0.5, s=b, ha="center", va="center", c="r", fontsize=14)

# 注释
plt.annotate(text="最高销量", xy=(3, 90), xytext=(1, 80), arrowprops={
    'width': 2,  # 箭头线的宽度
    'headwidth': 8,  # 箭头头部的宽度
    'facecolor': 'blue'  # 箭头的背景颜色
})

plt.title("折线")
# 自动调整布局
fig.tight_layout()
# plt.show()

plt.savefig(fname="pic.jpg", dpi=100, facecolor="pink", edgecolor="lightgreen", bbox_inches='tight', pad_inches=1)
