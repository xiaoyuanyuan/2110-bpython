import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# 让图片中可以显示中文
plt.rcParams['font.sans-serif'] = 'SimHei'
# 让图片中可以显示负号
plt.rcParams['axes.unicode_minus'] = False
fig = plt.figure(figsize=(14, 10))

df = pd.read_excel('../data/plot.xlsx', sheet_name='imshow')

# 产品数据
data = df.drop(columns="省份").values

x = df.drop(columns="省份").columns
# 省份数据
y = df.省份


print(data)
plt.imshow(data, cmap="Blues")

plt.xticks(range(len(x)), x)
plt.yticks(range(len(y)), y)

for i in range(len(x)):  # 列
    for j in range(len(y)):
        plt.text(x=i, y=j, s=data[j, i], ha="center", va="center")

plt.colorbar()

plt.show()
