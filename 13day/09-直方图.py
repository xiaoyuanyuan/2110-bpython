import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# 让图片中可以显示中文
plt.rcParams['font.sans-serif'] = 'SimHei'
# 让图片中可以显示负号
plt.rcParams['axes.unicode_minus'] = False
fig = plt.figure(figsize=(8, 5))

# x = np.random.randint(0, 10, 100)
# c = pd.Series(x).value_counts()
# print(c)
# plt.hist(x, bins=10)
# plt.hist(x, bins=[0, 3, 6, 9, 10])

df = pd.read_excel("../data/plot.xlsx", sheet_name="hist")
x = df["分数"]
print(x)
plt.hist(x, bins=range(40, 110, 6), facecolor='b', edgecolor='k', alpha=0.4)

# 自动调整布局
fig.tight_layout()
plt.show()
