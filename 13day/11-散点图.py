import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# 让图片中可以显示中文
plt.rcParams['font.sans-serif'] = 'SimHei'
# 让图片中可以显示负号
plt.rcParams['axes.unicode_minus'] = False
fig = plt.figure(figsize=(8, 5))

# x = range(1, 7, 1)
# y = range(10, 70, 10)
#
# plt.scatter(x, y)
#
# data = np.random.randn(100, 2)
# s = np.random.randint(50, 200, size=100)
# color = np.random.randn(100)
# plt.scatter(data[:, 0], data[:, 1], s=s, c=color, alpha=0.6)


df = pd.read_excel("../data/plot.xlsx", sheet_name="scatter")
x, y = df["广告费用"], df["销售收入"]
plt.figure(dpi=100)
# plt.scatter(x, y)

# plt.hexbin(x, y, gridsize=20, cmap="rainbow")

plt.xlabel("费用")
plt.ylabel("收入")

# 自动调整布局
fig.tight_layout()
plt.show()
