import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# 让图片中可以显示中文
plt.rcParams['font.sans-serif'] = 'SimHei'
# 让图片中可以显示负号
plt.rcParams['axes.unicode_minus'] = False
fig = plt.figure(figsize=(10, 8))
# 图形绘制
x = np.linspace(0, 10)
y = np.sin(x)
plt.plot(x, y)

plt.xlabel("y=sin(x)", fontsize=20, rotation=0)
plt.ylabel("y=sin(x)", rotation=90, ha="right")

plt.title("sin曲线")
# 自动调整布局
fig.tight_layout()

plt.show()
