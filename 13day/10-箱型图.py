import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# 让图片中可以显示中文
plt.rcParams['font.sans-serif'] = 'SimHei'
# 让图片中可以显示负号
plt.rcParams['axes.unicode_minus'] = False
fig = plt.figure(figsize=(8, 5))

# x = [1, 2, 3, 5, 7, 9, -10]
# plt.boxplot(x)

# x1 = np.random.randint(10, 100, 100)
# x2 = np.random.randint(10, 100, 100)
# x3 = np.random.randint(10, 100, 100)
#
# plt.boxplot([x1, x2, x3])
# plt.show()

data = np.random.normal(size=(500, 4))

lables = ['A', 'B', 'C', 'D']

plt.boxplot(data, notch=True, sym="go", labels=lables)

# 自动调整布局
fig.tight_layout()
plt.show()
