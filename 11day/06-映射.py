import pandas as pd
import numpy as np
from pandas import Series, DataFrame

index = ['鲁班', '张三丰', '张无忌', '杜甫', '李白']
columns = ['Python', 'Java', 'H5', 'Pandas']
# data = np.random.randint(5, 7, size=(5, 4))
data = np.random.randint(1, 100, size=(5, 4))

df = pd.DataFrame(data=data, index=index, columns=columns)
print(df)


# 替换元素
# print(df.replace({5: 10, 6: 20}))


# print(df["Python"].map({5: 10, 6: 20}))

# df["Numpy"] = df["Python"].map(lambda x: x + 10)
# df["Numpy"] = df["Python"] + 10
# print(df)

# df["是否及格"] = df["Java"].map(lambda x: "及格" if x > 60 else "不及格")
# print(df)

def fn(n):
    if n >= 80:
        return "优秀"
    elif n >= 60:
        return "及格"
    return "不及格"


df["是否及格"] = df["Java"].map(fn)
print(df)

# 替换行索引
print(df.rename({"鲁班": "鲁班大师"}))
# 替换列索引
print(df.rename({"Python": "PYTHON"}, axis=1))
