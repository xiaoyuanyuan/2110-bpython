import pandas as pd
import numpy as np
from pandas import Series, DataFrame

df = pd.DataFrame(data=np.random.randint(0, 10, size=(10, 3)),
                  index=list('ABCDEFHIJK'),
                  columns=['Python', 'NumPy', 'Pandas'])

print(df)
# 1、一列执行多项计算
# np.exp 求常数e的几次密
# print(df['Python'].transform([np.sqrt, np.exp]))  # Series处理)

#
# # 2、多列执行不同计算
def convert(x):
    print(x.mean())
    if x.mean() > 5:
        x *= 10
    else:
        x *= -10
    return x


# print(df.transform({'Python': convert, 'NumPy': np.max, 'Pandas': np.min}))
df.transform({'Python':convert,'NumPy':np.max,'Pandas':np.min})
