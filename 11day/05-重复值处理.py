import pandas as pd
import numpy as np
from pandas import Series, DataFrame


def make_df(indexs, columns):
    data = [[str(j) + str(i) for j in columns] for i in indexs]
    df = pd.DataFrame(data=data, index=indexs, columns=columns)
    return df


df = make_df([1, 2, 3, 4], ['A', 'B', 'C', 'D'])
print(df)
df.loc[1] = df.loc[2]
# print(df)
# print(df.duplicated())
# print(df.duplicated(keep="first"))  # 保留第一行
# print(df.duplicated(keep="last"))  # 保留最后一行
# print(df.duplicated(keep=False))  # 标记所有重复行
df.loc[1, "D"] = "DDD"
print(df)
# subset
# print(df.duplicated(subset=["A", "B", "C"]))

#  删除重复行
print(df.drop_duplicates())
print(df.drop_duplicates(subset=["A", "B", "C"]))
print(df.drop_duplicates(subset=["A", "B", "C"], keep="last"))
# 删除全部重复行
print(df.drop_duplicates(subset=["A", "B", "C"], keep=False))
