import pandas as pd
import numpy as np
from pandas import Series, DataFrame

data = np.random.randint(0, 100, size=(5, 5))
df = pd.DataFrame(data=data, columns=list('ABCDE'))
df.loc[2, "B"] = np.nan
df.loc[3, "C"] = None
print(df)

# print(df.isnull())  # 为空数据
# print(df.notnull())  # 不为空的数据

# any() 只要有一个True就返回True
# print(df.isnull().any())
# notnull 必须全部为True才为True
# print(df.notnull().all())

#  根据行判断
# print(df.isnull().any(axis=1))
# print(df.notnull().all(axis=1))


# cond = df.isnull().any(axis=1)
# print(cond)
# # ~ 取反  去掉有nan的行
# print(df[~cond])
# # 留下有nan的行
# print(df[cond])

# cond = df.notnull().all(axis=1)
# print(cond)
# #去掉有nan的行
# print(df[cond])


# 列过滤
# cond = df.notnull().all(axis=0)
# print(cond)
# # 去掉空列
# print(df.loc[:, cond])
# # 留下有空的列
# print(df.loc[:, ~cond])


cond = df.isnull().any()
print(cond)
# 去掉空列
print(df.loc[:, ~cond])
# 留下有空的列
print(df.loc[:, cond])
