import timeit

import pandas as pd
import numpy as np
from pandas import Series, DataFrame

# print(type(None))  # Object
# print(type(np.nan))  # float
#
# n = np.array([1, 2, 3, np.nan, 5, 6])
# print(n.sum())  # NAN
# print(np.nansum(n))  # 17

data = np.random.randint(0, 100, size=(5, 5))
df = pd.DataFrame(data=data, columns=list('ABCDE'))
print(df)
df.loc[2, "B"] = np.nan
# padans 做了优化，会把None变成Nan
df.loc[3, "C"] = None
print(df)
