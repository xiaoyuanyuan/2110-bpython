import pandas as pd
import numpy as np
from pandas import Series, DataFrame

data = np.random.randint(0, 100, size=(5, 5))
df = pd.DataFrame(data=data, columns=list('ABCDE'))
df.loc[2, "B"] = np.nan
df.loc[3, "C"] = None
print(df)

# print(df.fillna(value=100))
# print(df.fillna(value=100, limit=1))
df.fillna(value=100, limit=1, inplace=True)  # 在原数据上做tianchong
# print(df.fillna(method="ffill"))  # 用上面的数据填充
# print(df.fillna(method="bfill"))  # 用下面的数据填充
# print(df.fillna(method="bfill", axis=1))  # 用右边的数据填充
# print(df.fillna(method="ffill", axis=1))  # 用左边的数据填充
