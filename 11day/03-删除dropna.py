import pandas as pd
import numpy as np
from pandas import Series, DataFrame

data = np.random.randint(0, 100, size=(5, 5))
df = pd.DataFrame(data=data, columns=list('ABCDE'))
df.loc[2, "B"] = np.nan
df.loc[3, "C"] = None
print(df)
# print(df.dropna())  # 默认按 行删除有nan的行
# print(df.dropna(axis=1))  # 默认按 列删除有nan的列
# print(df.dropna(how="any"))  # 有nan就删
# print(df.dropna(how="all"))  # 全为nan就删


df2 = df.copy()
print(df2)
df2.dropna(inplace=True)  # 在原数据上做更改
print(df2)
