import pandas as pd
import numpy as np
from pandas import Series, DataFrame

df = pd.DataFrame(data=np.random.randint(0, 10, size=(5, 3)),
                  index=list('ABCDE'),
                  columns=['Python', 'NumPy', 'Pandas'])

print(df)


# Series调用
# print(df["Python"].apply(lambda x: True if x > 5 else False))
# DF调用 每一列的中位数
print(df.apply(lambda x: x.median()))
# print(df.apply(lambda x: x.median(), axis=1))


# def convert(x):
#     return (x.mean(), x.count())


# # 每一列的平均数
# print(df.apply(convert))

# DF专有方法
# print(df.applymap(lambda x: x + 100))
