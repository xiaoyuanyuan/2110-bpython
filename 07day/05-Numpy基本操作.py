import numpy as np

# array = np.random.randint(1, 10, size=(3, 5))
array = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
print(array)
print(array[0])
print(array[0][0])

# 根据列表不同索引访问方式
print(array[0, 0])
print(array[[0, 1, 1, 1]])
print(array[[0, 1, 1, 1]][0, 1])

array1 = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9])
print(array1[[0, 0, 0]])

# 根据bool值访问  数量跟array数量一致
b_list = [True, False, True, False, True, False, True, False, True]
print(array1[b_list])

# 赋值
array2 = np.array([1, 2, 3])
array2[2] = 10
print(array2)

array3 = np.array([[1, 2, 3], [1, 2, 3]])
array3[1][0] = 10
print(array3)
