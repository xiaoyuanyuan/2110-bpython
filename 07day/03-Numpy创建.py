"""
    Python三剑客:Numpy Pandas Matplotlib
    pip install numpy -i https://pypi.tuna.tsinghua.edu.cn/simple

    开发工具:
            1、idea做数据分析
            2、Jupyter 文本工具(能运行代码)

            安装：
               pip install Jupyter -i https://pypi.tuna.tsinghua.edu.cn/simple

            启动:
               jupyter notebook

"""
#  Numpy  数组   numpy array 或者nd.array
#  数组 [1,2,3,4,5]
#  列表 ["1",1,1.2]
#  numpy array

import numpy as np

# 创建一个numpy array 通过Python 列表创建
array = np.array([1, 2, 3])
print(array)

array1 = np.array(["1", "哈哈", 1])
print(array1.dtype)

# 指定数组中的数据类型
array2 = np.array([1, 2, 3], dtype=np.float32)
print(array2)

# 通过自身的api方式创建
# np.ones(shape, dtype=None, order='C')
# shape = (3,5) 3行5列
# shape = (3,) 一维
# shape = (3) -一维
# shape = (m,1) m行1列
# shape = (1,m) 1行m列
print(np.ones(shape=(3, 5), dtype=np.int32))
print(np.ones(shape=(3,), dtype=np.int32))
print(np.ones(shape=(3), dtype=np.int32))
print(np.ones(shape=(3, 1), dtype=np.int32))
print(np.ones(shape=(1, 3), dtype=np.int32))

# np.zeros(shape, dtype=float, order='C')
print(np.zeros(shape=(3, 5), dtype=np.int32))

# np.full(shape, fill_value, dtype=None, order='C')
print(np.full(shape=(3, 5), fill_value=6, dtype=np.float32))

# np.eye(N, M=None, k=0, dtype=float) 对角线为1其他的位置为0
print(np.eye(3))
print(np.eye(N=5, M=4))
print(np.eye(N=5, M=4, k=1))  # k为偏移量

# 等差数列 左闭右闭
# np.linspace(start, stop, num=50, endpoint=True, retstep=False, dtype=None)
print(np.linspace(1, 10))
print(np.linspace(1, 10, 10))

# 左闭右开
# np.arange([start, ]stop, [step, ]dtype=None)
print(np.arange(1, 10))
print(np.arange(1, 10, 2))

# 左闭右开
# np.random.randint(low, high=None, size=None, dtype='l')
print(np.random.randint(1, 10))
print(np.random.randint(1, 10, 10))
print(np.random.randint(1, 10, size=(3, 5)))

# np.random.randn(d0, d1, ..., dn) 标准正态分布　
print(np.random.randn(3))
# 二维
print(np.random.randn(3, 5))
# 三维
print(np.random.randn(3, 5, 6))

# np.random.normal() 普通正态分布
# 方差 5
print(np.random.normal(loc=180, scale=5))
print(np.random.normal(loc=180, scale=5, size=(3, 5)))

# np.random.random(size=None)  生成0到1的随机数，左闭右开
print(np.random.random(size=(3, 5)))


#np.random.permutation(10)
print(np.random.permutation(10))
