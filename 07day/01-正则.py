#  正则
import re

# 从左向右匹配

# . 任意一个字符 除了\n
# s = "\n13812345678"
# res = re.match("...", s)
# print(res)
# print(res.group())

# \w[a-z A-Z 0-9 _]
# s = "@aA9_"
# res = re.match(".\w+",s)
# print(res)

# \d 代表数字
# s = "123abc"
# res = re.match("\d\d\d",s)
# print(res)
# print(res.group())

# \D 代表非数字
# s = "abc123abc"
# res = re.match("\D\D\D",s)
# print(res)
# print(res.group())

# \S 匹配非空字符
# \s 匹配空字符
# s = " *123ABC"
# res = re.match("\s", s)
# print(res)

# [123] 匹配[]其中一个
# [^123] 匹配[]非其中内容
# s = "123"
# res = re.match("[^123]", s)
# print(res)

# 练习
# s = "13812345678"
# res = re.match("1[3-9]\d\d\d\d\d\d\d\d\d", s)
# print(res)

# 匹配数量的
# * 可有可无
# s = "13812345678234567823456782345678"
# res = re.match("1[3-9]\d*", s)
# print(res)
# + 至少有1个
# s = "13156782345678234"
# res = re.match("1[3-9]\d+", s)
# print(res)
# ? 要不0个 要不1个
# s = "1312"
# res = re.match("1[3-9]\d?", s)
# print(res)
# n个
# s = "13812345678"
# res = re.match("1[3-9]\d{9}", s)
# print(res)

# 至少n个
# s = "138123456781"
# res = re.match("1[3-9]\d{9,}", s)
# print(res)

# n-m 个
# s = "138123456781"
# res = re.match("1[3-9]\d{9,10}", s)
# print(res)

# 匹配开始  ^
# 匹配结尾  $

# s = "13812345678"
# res = re.match("^1[3-9]\d{9}$", s)
# print(res)

# 课堂练习
# s = "11#34123"
# res = re.match("\d+\.\d+",s)
# print(res)

# 原始字符串 字符串里面\n \t 正在写的时候一定要在前面加r
# s = "\\n哈哈\\n"
# # print(s)
# res = re.match(r"\\n.*\\n", s)
# print(res)
# print(res.group())

# 匹配邮箱 或
# s = "xxx@163.com"
# s1 = "xxx@qq.com"
# s2 = "_bama_ma@126.com"
# res = re.match("\w+@(126|163|qq)\.com", s2)
# print(res)

# 我今年18岁
# 我18岁
# 我的年龄18岁

# 提取18
# 正则是贪婪，关闭贪婪模式
# s = "我的年龄18岁"
# res = re.match(".*?(\d+)", s)
# print(res.group(0))
# print(res.group(1))

# s = "我的身高是1.75米，我的体重是100公斤"
# s1 = "我身高是1.75米，我体重是100公斤"
# s2 = "我身高是1.75米，我体重是100.2公斤"
# s3 = "我身高是1米，我体重是100.2公斤"
# 提取出1.75和100
# 用正则实现
# res = re.match(".*?(\d+\.*\d*).*?(\d+\.*\d*)", s)
# print(res)
# print(res.group(1))
# print(res.group(2))

# 创建正则的方式
# pattern = re.compile(".*?(\d+\.*\d*).*?(\d+\.*\d*)")
# res = pattern.match(s)  # 能一直复用
# print(res.group(1))
# print(res.group(2))

# s = "我的身高是1米，我的体重是100公斤"

# res = re.search("(\d+)",s)
# print(res.group(0))

# res = re.findall("(\d+)",s)
# print(res)

# res = re.split("\d+", s)
# print(res)

s = "123A"
s1 = "123a"

# re.I 忽略大小写
# re.S 支持换行
res = re.findall(".*?A", s1, re.I)
print(res)
