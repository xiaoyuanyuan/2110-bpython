import numpy as np

arr1 = np.random.randint(0, 10, size=(2, 3))
arr2 = np.random.randint(0, 10, size=(2, 3))
print(arr1)
print(arr2)
# 注意api，是np下面的
# axis控制级联的方向

print(np.concatenate((arr1, arr2)))  # 行连接
print(np.concatenate((arr1, arr2), axis=1))  # 列连接
print(np.concatenate((arr1, arr2), axis=0))

"""
    作业:
        1、用面向对象的方式封装pymysql,实现增删改查
        2、Numpy api 要完完整整的敲一遍
        3、jupyter安装，今晚自己解决一下。
        
        扩展：
            我建议安装到虚拟机 Anaconda和MiniAnaconda
            windows exe
            Linux  sh

"""
