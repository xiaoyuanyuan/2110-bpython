import numpy as np

# 变形
# 从1维度到多维
arr1 = np.random.randint(0, 10, size=(20))
print(arr1)
# print(arr1.reshape((4, 5)))
# print(arr1.reshape((2, 10)))
# print(arr1.reshape((2, 10, 1)))
# print(arr1.reshape((2, 5, 2)))
# print(arr1.reshape((3, 7)))

# 从多维度到1维
arr2 = np.random.randint(0, 10, size=(4, 5))
print(arr2)
print(arr2.reshape(20))
