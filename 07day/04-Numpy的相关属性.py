import numpy as np

array = np.random.randint(1, 10, size=(3, 5))
print(array)
print(array.ndim)  # 查看维度
print(array.shape)  # 查看形状
print(array.size)  # 总长度
print(array.dtype)  # 查看数据类型
print(type(array))  # 查看变量类型
