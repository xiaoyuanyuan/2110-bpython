t = (1, 2, 3, 4)
print(t[1:2])  # 能切

import numpy as np

# arr1 = np.array([1, 2, 3, 4, 5, 6])
# # 左闭右开
# print(arr1[0:3])
# print(arr1[-1:-4:-1])

arr2 = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
print(arr2)
print(arr2[0:2])  # 行切

print(arr2[:, 0:1])  # 列切


arr3 = np.random.randint(0, 10, size=(3, 4, 5))
print(arr3)
print(arr3[:, :, 0:1])

arr4 = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
print(arr4)
print(arr4[::-1])
print(arr4[:, ::-1])
