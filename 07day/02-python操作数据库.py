#  python操作数据库
"""
    pymysql  https://pypi.org/project/pymysql/
    安装：
        1、在pycharm安装 Python Interpreter 安装
        2、通过pip install 模块 -i https://pypi.tuna.tsinghua.edu.cn/simple

        3、指定版本安装
                pip install pymysql==1.0.2 -i https://pypi.tuna.tsinghua.edu.cn/simple
    查看安装过的模块
        pip freeze
        pip list
    卸载
        pip uninstall 模块
    查看模块信息
        pip show 模块
     导出安装的所有模块
        pip freeze > requirements.txt
     导入安装的所有模块
        pip install -r  requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple

"""
import pymysql

# 1.创建连接数据库
connect = pymysql.connect(host="localhost",
                          port=3306,
                          user="root",
                          password="123456",
                          database="db_16_01")

# 2.获取游标
cursor = connect.cursor()

# 3.编写sql
# sql = "select * from student"
sql1 = "select * from student where name=%s and age = %s"

# # 4.执行sql
# cursor.execute(sql1, args=("老赵", 20))
#
# # 5.获取结果
# res = cursor.fetchall()  # 获取全部
# res = cursor.fetchone() # 获取一个
# print(res)
#
# for r in res:
#     print(r)
#     print(r[0])

#  插入
sql = "insert into student values(%s,%s,%s,%s)"
cursor.execute(sql, args=(0, "wcc", 20, "男"))
connect.commit()  # 手动提交
# 更新
sql = "update student set name = %s where id=%s"
cursor.execute(sql, args=("王聪聪", 105))
connect.commit()

# 删除
sql = "delete from student where id = %s"
cursor.execute(sql, args=(106,))
connect.commit()
