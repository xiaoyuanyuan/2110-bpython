import numpy as np

# 切分和切片
# 切分返回多个
# 切片返回一个
# arr1 = np.random.randint(0, 10, size=(4, 6))

# print(arr1)

# indices_or_sections 分数
# axis 默认0
# print(np.split(arr1, indices_or_sections=2))  # 默认行切
# print(np.split(arr1, indices_or_sections=2, axis=1))

arr1 = np.random.randint(0, 10, size=(4, 6))
print(arr1)
# [3, 5] 相当于[0:3,3:5,5:] # 根据不同列数来切
print(np.split(arr1, indices_or_sections=[3, 5], axis=1))
