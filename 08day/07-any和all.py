# any 检测一个bool数组中，至少存在一个True,存在一个True 就返回True
# all  检测一个bool数组中，是否全为True 都为True,就返回True

import numpy as np

bool_list = np.array([False, False, False, False])
bool1_list = np.array([True, True, True, True])
print(np.any(bool_list))
print(np.all(bool1_list))

arr2 = np.random.randint(40, 100, size=(100))
print(arr2)
print(arr2 > arr2.mean())
print((arr2 > arr2.mean()).any())
print((arr2 > arr2.mean()).all())
