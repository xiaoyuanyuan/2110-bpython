import pymysql


class PyMysqlUtil():
    def __init__(self, auto_commit=True):
        self.connect = pymysql.connect(host="localhost",
                                       port=3306,
                                       user="root",
                                       password="123456",
                                       database="db_16_01")
        self.cursor = self.connect.cursor()
        self.auto_commit = auto_commit

    def query_all(self, sql, *args):
        self.cursor.execute(sql, args=args)
        return self.cursor.fetchall()

    def query_one(self, sql, *args):
        self.cursor.execute(sql, args=args)
        return self.cursor.fetchone()

    def insert(self, sql, *args):
        self.cursor.execute(sql, args=args)
        if self.auto_commit:
            self.commit()

    def update(self, sql, *args):
        pass

    def delete(self, sql, *args):
        pass

    def commit(self):
        self.connect.commit()
