import numpy as np

arr1 = np.random.randint(0, 10, size=(3, 5))
print(arr1)
# 复制  数据复制
arr2 = arr1.copy()
print(arr2)

# 修改原来的数据
arr1[0][0] = 100
print(arr1)
print(arr2)
