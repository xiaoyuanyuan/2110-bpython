import numpy as np

arr1 = np.random.randint(0, 10, size=(2, 3))
arr2 = np.random.randint(0, 10, size=(2, 3))

print(arr1)
print(arr2)
arr3 = np.hstack((arr1, arr2))
print(arr3)
arr4 = np.vstack((arr1, arr2))
print(arr4)

print(np.concatenate((arr1,arr2),axis=0))
