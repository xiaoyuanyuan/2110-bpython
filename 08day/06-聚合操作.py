import numpy as np

# 聚合
arr1 = np.random.randint(1, 10, size=(10))
# arr1 = np.random.randint(1, 10, size=(2,2))
print(arr1)
# 求和
print(arr1.sum())
# 求平均数  mean()
print(arr1.mean())
# 求最大值
print(arr1.max())
# 求最小值
print(arr1.min())
# 求最大值索引
print(arr1.argmax())
# 求最小值索引
print(arr1.argmin())
# 方差
print(arr1.var())
# 标准方差
print(arr1.std())
# 中位数 注意api  偶数个加在一起/2  奇数个就取中间
print(np.median(arr1))

# 统计出现的次数
print(np.bincount(arr1))

# 先统计出1-10出现的次数，在argmax求出出现次数最多 求众数
print(np.argmax(np.bincount(arr1)))

arr2 = np.random.randint(0, 10, size=(10))
print(arr2)
print(np.bincount(arr2))
print(np.argmax(np.bincount(arr2)))



# todo
data = np.arange(1,10,step=1)
print(data)
print(np.percentile(data, [0.25, 0.75]))
print(np.percentile(data, 50))
print(np.percentile(data, 25))
