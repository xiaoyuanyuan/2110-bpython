class Dog():
    i = 0  # 类属性  不会随着创建对象而发生改变

    def __init__(self, name):
        self.name = name
        Dog.i += 1

    def sleep(self):
        print("睡觉")


dog = Dog("A")
dog = Dog("B")
dog = Dog("C")
dog = Dog("D")

# 调用类属性
# 通过类调用  推荐
print(Dog.i)
# 通过对象调用
print(dog.i)
# print(dog.name)
