# Python  读写

# 写 w 	打开一个文件只用于写入。如果该文件已存在则打开文件，并从开头开始编辑，即原有内容会被删除。如果该文件不存在，创建新文件。
# f = open("1.txt", "w", encoding="utf-8")
# f.write("清明时节雨纷纷")
# f.close()

# w+ 打开一个文件用于读写。如果该文件已存在则打开文件，并从开头开始编辑，即原有内容会被删除。如果该文件不存在，创建新文件。
# f = open("1.txt", "w+", encoding="utf8")
# f.write("清明时节雨纷纷")
# f.seek(9)  # 一个中文3个字节
# print(f.read())
# f.close()

# 读  r
# f = open("1.txt", "r", encoding="utf8")
# print(f.read())
# f.close()

# 读写 r+
# f = open("1.txt", "r+", encoding="utf8")
# print(f.read())
# f.write("\n路人行人欲断魂")
# f.close()

# 追加a
# f = open("2.txt", "a", encoding="utf8")
# f.write("二月春风似剪刀")
# f.close()

# 追加 a+
# f = open("3.txt", "a+", encoding="utf8")
# f.write("二月春风似剪刀")
# f.seek(0)
# print(f.read())
# f.close()


# rb 读二进制
# f = open("test.jpg", "rb")
# content = f.read()
# f.close()

# wb 写入二进制
# f1 = open("test1.jpg", "wb")
# f1.write(content)
# f1.close()
