filename = input("请输入要复制的文件名字")
# 1.txt--->1_copy.txt
# 1.zz.txt--->1.zz_copy.txt

with open(filename, "r", encoding="utf8") as f:
    content = f.read()

# ["1","txt"]
# ["1.zz","txt"]
attr = filename.rsplit(".",1)
print(attr)
new_filename = attr[0] + "_copy." + attr[1]
with open(new_filename, "w", encoding="utf8") as f:
    f.write(content)

