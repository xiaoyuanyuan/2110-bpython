class Dog():
    # self 就是Java中的this
    def sleep(self):
        print("睡觉")

    def eat(self):
        print("吃")


dog = Dog()
# 调用方法
dog.sleep()
dog.eat()

# 赋值属性
dog.name = "旺财"
dog.age = 12
# 调用属性
print(dog.name)
print(dog.age)
