# 求完全数 :
# 6: 1 2 3
# 28: 1 2 4 7 14

# for i in range(2, 1000):
#     sum = 0
#     for j in range(1, i):
#         if i % j == 0:
#             sum += j
#     if sum == i:
#         print(i)

# 水仙花数 147 = 1^3+4^3+7^3
for i in range(100, 1000):
    # 123
    bai = i // 100
    shi = i // 10 % 10
    ge = i % 10
    if bai * bai * bai + shi * shi * shi + ge * ge * ge == i:
        print(i)

# 回文字符串
s = "aba"
if s == s[::-1]:
    print("回文")

# 1-2+3-4+5-98+99

# 求1-1000中的a^2+b^2=c^2 并a+b+c=1000的数字
for i in range(1, 1001):
    for j in range(1, 1001):
        k = 1000 - i - j
        if i * i + j * j == k * k and i + j + k == 1000:
            print(i)
            print(j)
            print(k)
            print("----")
        # for k in range(1, 1001):
        #     if i * i + j * j == k * k and i + j + k == 1000:
        #         print(i)
        #         print(j)
        #         print(k)
        #         print("----")

# .创建变量countries，赋值内容为"干一行行一行,一行行行行行,行行行干哪行都行",统计字符出现的次数,打印每个字符及出现的次数
from collections import Counter

countries = "干一行行一行,一行行行行行,行行行干哪行都行"
print(Counter(countries))

# 晚上作业:
#  1.学生管理系统改成面向对象版本
"""
#  2.老王开枪
#     人类
#         名字
           HP
#     枪类
#         名字
#     子弹
          名字
          攻击力
#     弹夹
          容量

引用：
    人引用枪 ，枪引用弹夹，弹夹引用子弹
    
流程
    1、创建老王 老宋
    2、创建一把枪
    3、创建一个弹夹
    4、创建多发子弹
    5、把子弹压入弹夹
    6、把弹夹装入枪中
    7、把枪给老王
    8、老王开枪射击老宋
    9、老宋掉血    
"""
