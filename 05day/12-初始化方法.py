class Dog():
    # 初始化属性方法 魔法方法 创建对象的时候自然会被调用
    def __init__(self, name, age, gender="公"):
        self.name = name
        self.age = age
        self.gender = gender

    # 打印对象的时候会被调用，必须要有返回值，返回值必须是字符串
    def __str__(self):
        return f"{self.name}+{self.age}+{self.gender}"

    def show(self):
        print("show")


dog = Dog("旺财", 12)
dog = Dog("旺财", 12, "母")
print(dog.name)
print(dog.age)
print(dog.gender)
print(dog)
