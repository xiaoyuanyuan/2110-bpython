class Home():
    def __init__(self, size, address, price):
        self.size = size
        self.address = address
        self.price = price
        self.beds = []  # 列表

    def renovation(self, bed):
        ares = 0
        for bed in self.beds:
            ares += bed.size

        # 剩余面积
        diff = self.size - ares
        if diff >= bed.size:
            self.beds.append(bed)
            print("装修成功")
        else:
            print("装修失败")


class Bed():
    def __init__(self, size, price):
        self.size = size
        self.price = price


home = Home(100, "中央大街", 100)
bed = Bed(12, 90)

home.renovation(bed)
home.renovation(bed)
home.renovation(bed)
home.renovation(bed)
home.renovation(bed)
home.renovation(bed)
home.renovation(bed)
home.renovation(bed)
home.renovation(bed)
