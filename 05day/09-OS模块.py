import os

print("1+2")
print(eval("1+2"))  # 把字符串解析成运算的式子

# 创建文件夹
# os.mkdir("TEST")
# r 代表原始字符串
# os.mkdir(r"C:\Users\Administrator\Desktop\zhuangao5\TEST")
# os.rmdir("TEST")
# 判断路径是否存在
# if not os.path.exists("TEST"):
#     os.mkdir("TEST")

# 替换 改名
# os.replace("TEST1", "TEST2")
# os.rename("TEST2", "TEST3")
# os.replace("1.txt", "11.txt")
# os.rename("11.txt", "22.txt")
# rename 目标文件存在，则报错
# os.rename("100.txt", "200.txt")
# replace 目标文件存在，则覆盖
# os.replace("100.txt", "200.txt")

# print(os.listdir("."))
# print(os.listdir("../05day"))
# for file in os.listdir("."):
#     if os.path.isdir(file):
#         print("文件夹")
#     elif os.path.isfile(file):
#         print("文件")

# 创建多层文件夹
# os.makedirs("./a/b")

# 获取当前所在路径
# print(os.getcwd())
# 切换路径
# os.chdir("../")
# print(os.getcwd())


import shutil
# 文件复制
shutil.copy("5.txt", "6.txt")
# shutil.copy("5.txt", "./a/b/6.txt")
# 批量复制 复制文件夹
# shutil.copytree("TEST3", "TEST4")
# shutil.copytree("a", "d")
