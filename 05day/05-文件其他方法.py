f = open("1.txt", "r", encoding="utf8")
# print(f.readline()) # 读一行
# print(f.readlines()) # 读多行
f.close()

# 上下文写法 ---面试可能考
# with ... as ...
with open("4.txt", "w", encoding="utf8") as f:
    f.write("隔江犹唱后庭花")

"""
    # 上下文
    def __enter__(self): # 进入
        print('Entering the block')
        return self
    def __exit__(self, exc_type, exc_val, exc_tb): # 离开
        print('Exiting the block')
"""

"""
   1、通过文件读写实现文件复制功能
   2、通过for循环实现斐波那契数列
   3、通过递归实现1-5的和

"""
