class ChinesePeople():
    gj = "中国"

    def __init__(self, name):
        self.name = name

    def eat(self):
        print("吃")

    @classmethod  # 类方法
    def introduce_gj(cls):
        print(cls.gj)


cp1 = ChinesePeople("张三")
cp2 = ChinesePeople("李四")
# 用类调用类方法 推荐
ChinesePeople.introduce_gj()
ChinesePeople.introduce_gj()
# 用对象调用
cp1.introduce_gj()
cp2.introduce_gj()
