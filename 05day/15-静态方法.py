import time


class Dog():
    i = 1  # 类属性

    def __init__(self, name):
        self.name = name  # 实例属性

    def show(self):
        print("实例方法")

    @classmethod
    def fun1(cls):
        print("类方法")

    # 跟对象没有关系，跟类关系，就用可以静态方法
    @staticmethod  # 静态方法
    def get_time():
        print(time.time())


dog = Dog("旺财")
dog.show()
Dog.fun1()
# 通过类调用静态方法
Dog.get_time()
# 通过对象调用静态方法
dog.get_time()

"""
    需求：买房需求
    定一个Home类，属性：大小100、地址、价格,记录装修床的历史记录
                 方法：装修(bed) 
    定一个Bed类，属性:大小（12），价格
    
    创建一个Home对象,在创建多个Bed对象，把Bed对象通过装修方法放到房建对象里面，
    装修(bed) 打印装修成功
    装修(bed) 打印装修成功
    装修(bed) 打印装修成功
    装修(bed) 打印装修成功
    装修(bed) 打印装修成功
    装修(bed) 打印装修成功
    装修(bed) 打印装修成功
    装修(bed) 打印装修成功
    装修(bed) 面积不足，装不下该床
"""
