import random


class Person():
    def __init__(self, name, hp):
        self.name = name
        self.hp = hp

    # 拿枪
    def use(self, ak47):
        self.gun = ak47

    # 开一次枪
    def shoot(self, laosong):
        # 弹子弹
        bullet = self.gun.p.pop()
        # 射人
        laosong.down_hp(bullet)

    # 掉血
    def down_hp(self, bullet):
        if bullet:
            if self.hp >= bullet.power:
                self.hp -= bullet.power
                print(f"{self.name}掉了{bullet.power},剩余{self.hp}")
            else:
                self.hp = 0
                print(f"{self.name}挂了{self.hp}")


class Gun():
    def __init__(self, name):
        self.name = name

    # 装弹夹
    def take(self, p):
        self.p = p


class Bullet():
    def __init__(self, name, power):
        self.name = name
        self.power = power


class Packet():
    def __init__(self, size):
        self.size = size
        self.bs = []

    # 装子弹
    def push(self, b):
        if self.size > len(self.bs):
            self.bs.append(b)
        else:
            print("装不下了")

    # 弹子弹
    def pop(self):
        if self.bs:
            return self.bs.pop(-1)
        else:
            print("弹夹空了")


# 创建老王老宋
laowang = Person("老王", 100)
laosong = Person("老宋", 100)
# 创建一把枪
ak47 = Gun("ak47")
# 创建一个弹夹
p = Packet(35)
# 创建多发子弹
for i in range(35):
    b = Bullet("7.62", random.randint(1, 10))
    # 把子弹压入弹夹
    p.push(b)
# 把弹夹装入枪中
ak47.take(p)
# 把枪给老王
laowang.use(ak47)
# 老王开枪

for i in range(40):
    laowang.shoot(laosong)

# 创建一把枪
m16 = Gun("m16")
# 创建一个弹夹
p1 = Packet(30)
# 创建多发子弹
for i in range(30):
    b1 = Bullet("5.56", random.randint(1, 10))
    # 把子弹压入弹夹
    p1.push(b1)
# 把弹夹装入枪中
m16.take(p1)
# 把枪给老王
laosong.use(m16)

# 互相射
# for i in range(50):
#     num = random.randint(0, 1)
#     if num == 0:
#         laowang.shoot(laosong)
#     else:
#         laosong.shoot(laowang)
