"""
    装饰器
    主要解决已经实现的功能不要改动，而是对外扩展。

    工作场景
        1、日志打印
        2、鉴权
        3、额外的功能

"""


def fun(f):
    def inner():
        print("打印日志")
        f()
        print("打印日志")

    return inner


@fun  # f---login  login---->inner
def login():
    # 打印日志
    print("登录功能")
    # 打印日志


# login()
# --------------------------------------------


# def fun1(f1):
#     def inner1(*args, **kwargs):
#         print("打印日志")
#         f1(*args, **kwargs)
#         print("打印日志")
#
#     return inner1
#
#
# @fun1
# def login1(account, pwd):
#     print("登录功能" + account + pwd)
#
#
# login1("123", "123")


# -----------------------------有返回值

# def fun2(f2):
#     def inner2(*args, **kwargs):
#         print("打印日志")
#         res = f2(*args, **kwargs)
#         print("打印日志")
#         return res
#
#     return inner2
#
#
# @fun2
# def login2(account, pwd):
#     print("登录功能" + account + pwd)
#     return "登录成功"
#
#
# res = login2("123", "123")
# print(res)

# ---------------------- 带这个传参数的装饰器


# def fun4(type):
#     def fun3(f3):
#         def inner3(*args, **kwargs):
#             if type == 0:
#                 print("打印日志")
#                 res = f3(*args, **kwargs)
#                 print("打印日志")
#             else:
#                 print("打印日志1")
#                 res = f3(*args, **kwargs)
#                 print("打印日志1")
#             return res
#
#         return inner3
#
#     return fun3
#
#
# @fun4(type=0)
# def login3(account, pwd):
#     print("登录功能3" + account + pwd)
#     return "登录成功"


# def fun4(f4):
#     def inner4(*args, **kwargs):
#         print("打印日志1")
#         res = f4(*args, **kwargs)
#         print("打印日志1")
#         return res
#
#     return inner4

# @fun4(type=1)
# def login4(account, pwd):
#     print("登录功能3" + account + pwd)
#     return "登录成功"
#
#
# res = login3("123", "123")
# print(res)
#
# res1 = login4("123", "123")
# print(res1)


# 两个装饰器

def fun5(f):
    def inner():
        return "1" + f() + "1"

    return inner


def fun6(f):
    def inner():
        return "2" + f() + "2"

    return inner


@fun5  # 后装饰它
@fun6  # 先装饰它
def test():
    return "h"


print(test())
# h1
# 21h21
# 12h12
# 2h2
# 1h1
# 21h12
# 12h21
