# 递归函数--->自己调用自己
# 阶乘： 5!=5*4*3*2*1
# 函数
# def test(num):
#     ji = 1
#     for i in range(num, 1, -1):
#         ji *= i
#     return ji
#
#
# print(test(5))

# 递归
# def a():
#     print("a")
#     a()
# a()

def test1(num):
    if num == 1:
        return 1
    return num * test1(num - 1)


# test1(5)  # 5 * 24
# test1(4)  # 4 * 6
# test1(3)  # 3* 2
# test1(2)  # 2* 1
# test1(1)  # 1

print(test1(5))


# 斐波那契数列
#  1 1 2 3 5 8 13 21 34  55
#  递归
def test2(num):
    if num == 1 or num == 2:
        return 1
    return test2(num - 1) + test2(num - 2)


print(test2(10))  # 第10个斐波那契数是多少
# test2(5) # test2(4)+test2(3)
# test2(4) # test2(3)+test2(2)
# test2(3) # test2(2)+test2(1)
# test2(2)  #1
# test2(1)  # 1
# test2(2)  # 1
#
# test2(3)  #test2(2)+test2(1)
# test2(1)  # 1
# test2(2)  # 1
