# 写一个学生管理系统，用字典、列表实现增删改查

stus = [{"name": "a", "birthday": "1990-06-22", "gender": "女"},
        {"name": "b", "birthday": "1990-06-22", "gender": "女"},
        {"name": "c", "birthday": "1990-06-22", "gender": "女"}]  # 装数据


def show_welcome():
    print("欢迎使用学生管理系统".center(20, "*"))


"""
    添加
"""


def add(name, birthday, gender):
    # 可变类型不需要声明global
    d = {"name": name, "birthday": birthday, "gender": gender}
    stus.append(d)


def query(name):
    for stu in stus:
        if stu.get("name") == name:
            print(f"姓名：{stu['name']} 年龄是{stu['birthday']},性别是{stu['gender']}")
            break
    else:
        print("找不到")


def delete(name):
    for stu in stus:
        if stu.get("name") == name:
            stus.remove(stu)
            break
    else:
        print("找不到")


def update(name):
    for stu in stus:
        if stu.get("name") == name:
            while True:
                print("1.修改名字")
                print("2.修改生日")
                print("3.修改性别")
                print("4.退出修改")
                num = int(input("请输入序号"))
                if num == 1:
                    new_name = input('请输入姓名')
                    stu["name"] = new_name
                elif num == 2:
                    new_birthday = input('请输入生日')
                    stu["birthday"] = new_birthday
                elif num == 3:
                    new_gender = input('请输入性别')
                    stu["gender"] = new_gender
                else:
                    break
            break
    else:
        print("找不到")


def print_all():
    for stu in stus:
        print(f"姓名：{stu['name']},年龄:{stu['birthday']},性别:{stu['gender']}\n")


def menu():
    while True:
        print("1.增加学生")
        print("2.查找学生")
        print("3.修改学生")
        print("4.删除学生")
        print("5.打印学生")
        print("6.退出系统")
        num = int(input('请输入功能序号'))
        if num == 1:
            name = input('请输入学生姓名')
            birthday = input('请输入学生出生年月')
            gender = input('请输入学生性别')
            add(name, birthday, gender)
            print("添加成功")
        elif num == 2:
            name = input("请输入要查询的学生的姓名")
            query(name)
        elif num == 3:
            name = input("请输入要查询的学生的姓名")
            update(name)
        elif num == 4:
            name = input("请输入要删除的学生的姓名")
            delete(name)
        elif num == 5:
            print_all()
        elif num == 6:
            break

menu()
