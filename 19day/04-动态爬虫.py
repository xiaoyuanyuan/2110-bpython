"""
    动态爬虫：通过模拟浏览器，来模拟人的行为

    模拟浏览器：Selenium

    搜百度操作：打开浏览器--->输入搜索内容---->回车搜索

    安装：pip install selenium -i 镜像源

    驱动下载地址：http://chromedriver.storage.googleapis.com/index.html
    驱动可以用改框架代替：webdriver_manager

    参考地址：https://www.cnblogs.com/sunjump/p/16529035.html


"""
# 导入selenium
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
options = webdriver.ChromeOptions()
options.add_experimental_option('detach', True)

# 选择谷歌浏览器
driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)
# 打开浏览器，如果一个地址
driver.get("https://www.baidu.com/")
# 放大浏览器
driver.maximize_window()
# 定位输入框
driver.find_element_by_id("kw").send_keys("PYTHON")
# 定位到百度一下
driver.find_element_by_id("su").click()
