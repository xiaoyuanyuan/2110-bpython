# 编写一个Python程序，接受用户输入的两个数字，计算它们的最小公倍数
# a = int(input('请输入第一个数字：'))
# b = int(input('请输入第二个数字：'))
# n = a
# m = b
# # 额外的变量
# t = a
# while (t > 0):
#     if (a % t == 0 and b % t == 0):
#         break
#     t = t - 1
# max = t
# # 最小公倍数=两数的乘积/最大公约(因)数
# min = m * n / t
# print('最大公约数是：', max)
# print('最小公倍数是：', min)

# 编写一个Python程序，使用列表推导式打印99乘法表
# 1 *1 =1
# 2*1 = 2  2* 2
# [["1*1=1"],["2*1"=2,"2*2"=4],[],[],[],[]]
# l = [[f"{i}*{j}={i * j}\t" for j in range(1, i + 1)] for i in range(1, 10)]
# s = ""
# for i in l:
#     s += "".join(i)
#     s += "\n"
# print(s)

#编写一个Python程序，生成一组传感器温度采集数据，包含传感器编号，温度和采集时间。要求温度为80-120之间随机数，时间使用当前系统时间。(2分编写一个Python程序，生成一组传感器温度采集数据，包含传感器编号，温度和采集时间。要求温度为80-120之间随机数，时间使用当前系统时间。(2分
import random
from datetime import datetime


# 定义生成温度的函数，范围在80到120之间
def generate_temperature():
    return random.randint(80, 120)


# 生成数据的函数
def generate_sensor_data(num_sensors=1):
    data = []
    for i in range(1, num_sensors + 1):
        # 生成温度
        temperature = generate_temperature()
        # 获取当前系统时间
        current_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        # 创建数据字典
        sensor_data = {
            'sensor_id': i,
            'temperature': temperature,
            'timestamp': current_time
        }
        # 将数据添加到列表中
        data.append(sensor_data)
    return data


# 示例：生成5个传感器的数据
sensor_data_list = generate_sensor_data(5)
# 打印数据
for data in sensor_data_list:
    print(f"Sensor ID: {data['sensor_id']}, Temperature: {data['temperature']}, Timestamp: {data['timestamp']}")