"""
    做公司，基本上中台或者平台
    框架： hadoop flume kafka datax spark flink sqoop zk zabbix datavines  persto Kylin
    平台： 自己封装利用Java+框架     付费平台 如 网易大数据、星环大数据、阿里云大数据

    大数据和后端分不开，数据要展示，通过接口，学会调用

    接口调用： Java或Python
"""
import requests

headers = {
    "Content-Type": "application/x-www-form-urlencoded"
}
params = {
    "key": "bc58e72f0943773592f140db1511f57e",
    "consName": "金牛座",
    "type": "today"
}
response = requests.get("http://web.juhe.cn/constellation/getAll", headers=headers, params=params)

print(response.json())
