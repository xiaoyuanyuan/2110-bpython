# set  数据不可重复
s1 = {1, 2, 3, 4, 4}
print(type(s1))
print(s1)

# api
s2 = set({})
print(type(s2))
s3 = {1, 2, 3}
# 添加
# s3.add(4)
# print(s3)
# 删
# s3.remove(4) # 不存在报错
# print(s3)

# s3.discard(4) Remove an element from a set if it is a member.
# print(s3)

s1 = {1, 2, 3, 5}
s2 = {1, 2, 3, 4}
# 交集
print(s1 & s2)
# 并集
print(s1 | s2)
# 差集
print(s1 - s2)
print(s2 - s1)
