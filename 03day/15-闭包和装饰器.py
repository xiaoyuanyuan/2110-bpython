#  闭包：函数嵌套，并且内部函数用到了外部函数的变量

# def a(args):
#     def b():
#         print(args)
#     return b
# b = a(1)
# b()

# 装饰器
# 开放封闭原则:已经实现的功能不要修改，而是对外扩展
# 在调用的支付时候，要检查是否登录

# def islogin():
#     print("true")
#
#
# def pay():
#     if islogin():
#         print("支付成功")
#
# if islogin():
#     pay()

import random


def fun1(f):
    def fun2():
        if random.randint(0, 1) == 0:
            f()
        else:
            print("未登录")

    return fun2


@fun1  # 语法糖 ---蘑菇头
def pay():
    print("支付成功")


# pay()

# a = fun1(pay) # f--->pay()  a--->fun2()
# a()

# ----
# pay = fun1(pay) # f--->pay()  pay--->fun2()
# pay()

# 原来 pay--->pay()  fun1--->fun1()  fun2---fun2()
# pay = fun1(pay)  f--->pay()  pay--fun2()  fun1---fun1()
# pay()


pay()  # @fun1相当于----pay = fun1(pay)



