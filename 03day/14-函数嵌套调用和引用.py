# 函数嵌套调用
def b():
    print("b")


def a():
    print("a")
    b()


a()

# 函数引用
def c():
    print("c")

d = c
d()
c()

