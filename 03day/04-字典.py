# HashMap底层:数组+链表
# Dict: KEY-->VALUE   Key不能重复
d = {}  # 空字典
d = {"name": "张三", "年龄": 22, "性别": "男"}
# 增删改查
# 增
# d["爱好"] = "抽烟喝酒烫头"
# print(d)
# # 查
# print(d["name"])
# # 改
# d["name"] = "张四"
# print(d)
# # 删
# d.pop("name")
# print(d)

# 额外的api
# d1 = d.copy()  # 副本复制
# d1 = d  # 引用复制
# d["name"] = "张思"
# print(d1)

# d.clear()
print(d.keys())  # 获取所有的键
print(d.values())  # 获取所有的值
print(d.items())  # 获取所有的键值对
for k in d.keys():
    print(k)
for v in d.values():
    print(v)
for item in d.items():
    print(item)
    print(item[0])
    print(item[1])
for k, v in d.items():
    print(k, v)

# a, b = [1, 2]
# a, b = 1, 2
# a, b = (1, 2)

a, b = 10, 20  # 把a,b的值调换
# a,b = b,a

# c = a
# a = b
# b = c

# a = a + b  # 10+20 = 30
# b = a - b  # 30 -20 = 10   b = 10
# a = a - b  # 30-10 = 20    a = 20


print(d.get("name1", "zs"))  # 键不存在返回None,可以填写默认值
# 区别
# print(d["name1"])  # 键不存在报错
# 随即删除一对键值对
# d.popitem()
# print(d)


d1 = {"a": 1, "b": 2}
# d1.setdefault("a1", "10")  # 存在不更新，不存在更新
# print(d1)
d2 = {"c": 3, "d": "4", "a": 10}
d1.update(d2)  # 两个字典合并
print(d1)

