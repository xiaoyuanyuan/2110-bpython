"""
    1、可变数据类型：list dict set
    2、不可变数据类型：int str bool float tuple
    3、非零即真、非空即真, 空元组、空列表、空字典、空集合这些转成布尔都是False
    4、列表和元组可以相互转换
    5、列表和集合相互转换
    6、字典的key不能是可变类型数据
"""
i = 1
print(id(i))  # id返回i在内存中的位置
i = 10
print(id(i))

l = [1]
print(id(l))
l.append(10)
print(id(l))

print(bool([]))
print(bool({}))
print(bool(()))
print(bool(set({})))

l = [1, 2, 3]
print(tuple(l))
t = (1, 2, 3)
print(list(t))

l1 = [1, 2, 3, 3]
print(set(l1))  # 去重
s = {1, 2, 3, 3}
print(list(s))

d = {1: 2}
d = {"1": 2}
d = {1.2: 2}
d = {True: 2}
d = {(1, 2, 3): 2}

# d = {[1, 2, 3]: 2} #key不能list
# d = {{1: 2}: 2} # key不能dict
# d = {{1, 2, 3}: 2}  # key 不能set


l = []
if l:
    print("true")
else:
    print("false")

if not l:
    print("空列表")
