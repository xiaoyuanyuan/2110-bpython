# 函数参数
# 位置参数
def c(a, b):  # 形参
    print(a + b)


# 实参
c(1, 2)
c(a=1, b=2)  # 关键字传参法
c(b=2, a=1)  # 关键字传参法


# 默认参数
def c1(a, b=1):
    print(a + b)


# def c1(b=1, a):  # 默认参数必须在后面
#     print(a + b)


c1(1, 2)  # 传参数，用传递的
c1(1)  # 没传参数，就用默认的
c1(a=1, b=2)
c1(b=2, a=1)
c1(a=1)


# c1(b=2)  #报错

# 不定长参数
def c2(a, b, *args):
    print(a)
    print(b)
    print(args)


c2(1, 2)
c2(1, 2, 3, 4, 5, 6, 7, 8)


# 不定长和默认参数一起用
def c3(a, b=1, *args):
    print(a)
    print(b)
    print(args)


c3(1, 2)
c3(1, 2, 3, 4, 5, 6)
c3(a=1)
c3(a=1, b=1)
c3(a=1, b=1)


# 改版一下
def c4(a, *args, b=1):
    print(a)
    print(b)
    print(args)


c4(1, 2, 3, 4, 5, b=2)


# 关键字不定长

def c5(a, b, *args, c=2, **kwargs):
    print(a)
    print(b)
    print(args)  # (1,2,3,4)
    print(c)
    print(kwargs)  # 字典

    # 课堂练习，求a+b+args+c+kwargs的和
    # print(a + b + c+sum(args)+sum(kwargs.values()))

    sum = a + b + c
    for v in args:
        sum += v
    for v in kwargs.values():
        sum += v
    print(sum)


# c5(1, 2, 3, 4, 5, 6, c=8, d=2, f=3)

t = (7, 8, 9)
d = {"d": 2, "f": 3}
# 拆包
c5(1, 2, 3, 4, c=5, *t, **d)

print()
