# 函数返回值
def fun(a, b):
    return a + b
    # print(a+b)


res = fun(1, 2)
print(res)

s = "1,2,3"


# s.split(",")
# l = [1,2,3]
# l.sort()

# 返回多个值

def fun1(a, b):
    return a + b, a - b


res = fun1(1, 2)
print(res)  # 返回元组
r1, r2 = fun1(1, 2)
print(r1)
print(r2)
