import time

# 时间戳--->日期
# 日期---->时间戳
print(int(time.time() * 1000))

timestamp = 1615132800  # 时间戳
date = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(timestamp))  # 标准UTC
print(date)

timestamp = 1713771649237  # 假设给定的毫秒时间戳为1597058226068
date = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(timestamp / 1000))  # 中国时间
print(date)


# 日期转时间戳
import datetime
now = datetime.datetime.now()
print(now)
timestamp = now.timestamp()
print("当前时间戳为：", timestamp)



# 定义日期时间字符串
date_str = "2024-04-22 15:43:56"
# 将字符串转换为datetime对象
date_obj = datetime.datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S")
# 转换datetime对象为时间戳（以秒为单位）
timestamp = int(date_obj.timestamp())
print(timestamp)
