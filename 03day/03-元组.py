# tuple  不可变
t1 = ()  # 空元组
print(type(t1))

t2 = (1,)  # 当元组只有一个数据的时候，需要加上,
print(type(t2))

t3 = (1, 2, 3)
print(t3.count(2))
print(t3.index(2))

# 元组和列表的不同点
# 元组不可变，列表可变
# 工作场景： x = ("A","B","O","AB")
