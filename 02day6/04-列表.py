# 列表不是数组，像数组一样类型
#  Java类型数组什么特点 长度固定，类型固定，有序，可重复，空间地址是连续的，ArrayList(数组)  LinkedList(双向链表)
#  ArrayList 读快，随机删除插入慢，往最后一个插入特殊
#  LinkedList 随机删除插入快，读的慢

# 列表 长度不固定，类型不固定，有序，可重复

# List 列表

# 声明空列表
l = []
# 增删改查
# 增(insert append extend add put)
l.append(1)  # 向后追加
l.append("老王")  # 向后追加
print(l)
# 查
print(l[0])
# 改
l[0] = "老宋"
print(l)
# 删
l.remove("老宋")
print(l)

