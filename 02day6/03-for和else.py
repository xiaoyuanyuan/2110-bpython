# for else
# for循环如果正常的死亡，会执行， 非正常 如break，else不会执行
for i in range(0, 100):
    print(i)
    if i == 99:
        break
else:
    print("哈哈")
