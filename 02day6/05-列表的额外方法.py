#  额外的扩展方法
l = ["小王", "小李", "小赵"]
l.insert(0, "小爱")
l1 = ["小红", "小绿", "小黄"]
l.extend(l1)
print(l)

# 删除
l = ["1", "2", "3", "1"]
# l.remove("1") # 按值
# l.pop(2)  # 根据索引
del l[0]  # 不属于列表的方法
print(l)

l = [1, 2, 4, 0, 8, 6, 2]
print(l.index(2))
print(l.count(2))
# l.clear()
# l.sort()  # 升序
# l.sort(reverse=True)  # 降序，在原列表操作
# print(l)
# l1 = l.copy()  # 副本 修改原数据，不会影响新数据
# l[0] = 100
# print(l)
# print(l1)

l.reverse()  # 反转
print(l)

# 列表遍历
l = ["a", "b", "c"]
for i in l:  # java ---foreach
    print(i)
for i in range(0, len(l)):  # java---length
    print(l[i])

# 公共方法
# len()
# sum()
# max()
# min()
# sorted()
l = [1, 2, 3, 6, 5]
print(len(l))
print(max(l))
print(min(l))
print(sum(l))
print(sorted(l))  # 升序，返回一个新的列表
print(sorted(l, reverse=True))  # 降序，返回一个新的列表
print(reversed(l).__next__())  # 倒序

# 二维
l = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
for i in l:
    for j in i:
        print(j)

# 判断值在不在列表当中
# l = ["1", "2", "3"]
# i = input("请输入一个数字")
# if i in l:
#     print("ZAI")
# if i not in l:
#     print("BU ZAI")

# print(l.index("10"))
# print(l.count("10"))

# 切片
l = ["1", "2", "3", "4"]
# l["start":"end":"step"] start-->0  step-->1
print(l[:2])
print(l[0:2])
print(l[0:2:1])
print(l[0:4:2])
print(l[-1])
print(l[-1:-3:-1])
print(l[::-1])  # 反转
