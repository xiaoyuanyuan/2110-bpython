# 求2-100 质数 只能被1和本身整除
for i in range(2, 101):
    # 业务逻辑
    # 19
    # 19%2==0
    # 19%3==0
    # 19%18==0
    # flag = True
    # for 正常结束，证明，break没执行，break没执行，代表这个数就是质数
    for j in range(2, i):
        if i % j == 0:
            # print("{}不是质数".format(i))
            flag = False
            break
    # if flag:
    #     print(i)
    else:
        print(i)

# for i in range(2, 100):
#     if (i % 2 == 0 or i % 3 == 0 or i % 5 == 0 or i % 7 == 0):
#         print(i)
