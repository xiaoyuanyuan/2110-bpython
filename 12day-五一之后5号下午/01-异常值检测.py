import pandas as pd
import numpy as np
from pandas import Series, DataFrame

df = pd.DataFrame(data=np.random.randint(0, 10, size=(10, 3)),
                  index=list('ABCDEFHIJK'),
                  columns=['Python', 'NumPy', 'Pandas'])

# print(df)
# 每一列相关性的统计值
# print(df.describe())
# print(df.describe().T)
# print(df.describe([0.3, 0.9, 0.6, 0.99]).T)

# 标准差=方差开平方
# print(df.std())
# print(df.std(axis=1))

# df3 = df.copy()
# print(df3)
# 默认按行删除
# print(df3.drop("A"))
# # 删除列
# print(df3.drop("Python", axis=1))

# 与上面等价
# print(df3.drop(index="A"))
# print(df3.drop(columns="Python"))

# 删除多个
# print(df3.drop(columns=["Python", "NumPy"]))
# print(df3.drop(index=["A", "B"]))

index = ['鲁班', '张三丰', '张无忌', '杜甫', '李白']
columns = ['Python', 'Java', 'H5', 'Pandas']
data = np.random.randint(0, 10, size=(5, 4))
df1 = pd.DataFrame(data=data, index=index, columns=columns)
# print(df1)

# 去重
# print(df1["Python"].unique())


# 按条件查询
# print(df1.query("Python == 5"))
# print(df1.query("Python > 5"))
# print(df1.query("Python == 5 and Java== 5"))
# print(df1.query("Python == 5 & Java== 5"))
# print(df1.query("Python == 5 or Java== 5"))
# print(df1.query("Python == 5 | Java== 5"))

# i = 5
# print(df1.query("Python == @i"))
# print(df1.query("Python in [3,4,5,6]"))
# l = [3, 4, 5, 6]
# print(df1.query("Python in @l"))


index = ['鲁班', '张三丰', '张无忌', '杜甫', '李白']
columns = ['Python', 'Java', 'H5', 'Pandas']
data = np.random.randint(0, 100, size=(5, 4))
df2 = pd.DataFrame(data=data, index=index, columns=columns)

print(df2)
# print(df2.sort_values("Python"))
# print(df2.sort_values("Python", ascending=False))
# print(df2.sort_values("李白", axis=1))


# 按行索引排序
print(df2.sort_index())
print(df2.sort_index(ascending=False))
# print(ord("张"))
# print(ord("李"))

# 按列索引排
# print(df2.sort_index(axis=1))
# print(df2.sort_index(axis=1, ascending=False))


print(df2.info())