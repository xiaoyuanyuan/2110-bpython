import pandas as pd
import numpy as np
from pandas import Series, DataFrame

df = pd.DataFrame(np.random.randn(10000, 3))
print(df)
# print(df.head())
# print(df.tail())

# 求标准差
s = df.std()
#  绝对值
df1 = df.abs()

cond = df1 > s * 3
print(cond)

# 判断每一行是否有true
cond2 = cond.any(axis=1)

# 删除大于3倍标准差的行
print(df1.loc[~cond2])
