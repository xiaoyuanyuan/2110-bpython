import pandas as pd
import numpy as np
from pandas import Series, DataFrame

# 创建DataFrame
df = pd.DataFrame(
    {
        'color': ['green', 'green', 'yellow', 'blue', 'blue', 'yellow', 'yellow'],
        'price': [4, 5, 3, 2, 7, 8, 9]
    }
)
# print(df)
# print(df.groupby(by="color").groups)
# print(df.groupby(by="color").sum())
# print(df.groupby(by="color").mean())


# 课堂练习

ddd = pd.DataFrame(
    data={
        "item": ["萝卜", "白菜", "辣椒", "冬瓜", "萝卜", "白菜", "辣椒", "冬瓜"],
        'color': ["白", "青", "红", "白", "青", "红", "白", "青"],
        'weight': [10, 20, 10, 10, 30, 40, 50, 60],
        'price': [0.99, 1.99, 2.99, 3.99, 4, 5, 6, 7]
    }
)
print(ddd)

# print(ddd.groupby(by="color").sum())
# print(ddd.groupby(by="color")["price"].sum())
# print(ddd.groupby(by="color")["price"].sum().loc[["白"]])
# print(ddd.groupby(by="color")["price"].sum().loc[["白", "红"]])
# print(ddd.groupby(by="item")["weight"].sum())
# print(ddd.groupby(by="item")["price"].mean())
# print(ddd.groupby(by="item")["price"].mean().loc["萝卜"])


# 小技巧
# 返回series类型
# df1 = ddd.groupby(by="item")["weight"].sum()

# 返回dataframe类型
df1 = ddd.groupby(by="item")[["weight"]].sum()
df2 = ddd.groupby(by="item")[["price"]].mean()

print(type(df1))

# print(df1)
# print(df2)

# print(df1+df2)

print(df1.merge(df2, left_index=True, right_index=True))
