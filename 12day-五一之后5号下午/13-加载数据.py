import pandas as pd
import numpy as np
from pandas import Series, DataFrame

# data = np.random.randint(0, 50, size=(10, 5))
# df = pd.DataFrame(data=data, columns=['Python', 'Java', 'Go', 'C', 'JS'])

# df.to_csv("data.csv", sep=",", header=True, index=True)


# df = pd.read_csv("data.csv", sep=",", header=[0], index_col=0)
# print(df)

# df = pd.read_table('data.csv', sep=",", header=0, index_col=0)
# print(df)


# ------------------------
data = np.random.randint(0, 50, size=(10, 5))
# df = pd.DataFrame(data=data, columns=['Python', 'Java', 'Go', 'C', 'JS'])

# df.to_excel("data.xls", sheet_name="sheet3", header=True, index=True)
# names替换列索引
df1 = pd.read_excel("data1.xls", sheet_name="Sheet1", header=0, names=list("ABCDE"), index_col=1)
print(df1)
