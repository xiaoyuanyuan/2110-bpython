import pandas as pd
import numpy as np
from pandas import Series, DataFrame

df = pd.DataFrame(data=np.random.randint(0, 100, size=(20, 3)))
print(df)

# print(df.count())
# print(df.count(axis=1))
# print(df.max())
# print(df.min())
# print(df.max(axis=1))
# print(df.min(axis=1))

# print(df.median())
# print(df.median(axis=1))

# 每一列元素出现的个数
# print(df[0].value_counts())

# 累加
# print(df.cumsum())
# 累乘
# print(df.cumprod())

# print(df.var())
# print(df.std())

# 协方差 协方差>0 : 表式两组变量正相关  协方差<0 : 表式两组变量负相关  协方差=0 : 表式两组变量不相关
# print(df.cov())
# print(df[0].cov(df[1]))

# 相关系数r＞0为正相关，r＜0为负相关。r＝0表示不相关
print(df.corr())
print(df.corrwith(df[2]))
