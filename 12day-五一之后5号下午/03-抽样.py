import pandas as pd
import numpy as np
from pandas import Series, DataFrame

df = pd.DataFrame(data=np.random.randint(0, 10, size=(10, 3)),
                  index=list('ABCDEFHIJK'),
                  columns=['Python', 'NumPy', 'Pandas'])
print(df)
# 使用前面的df2
# print(df.take([4, 1, 2, 3, 0]))  # 行排列
# print(df.take([0, 2, 1], axis=1))  # 列排列
# 随即排列
# print(df.take(np.random.permutation([0, 1, 2, 3])))

# 有放回抽样
# print(df.take(np.random.randint(0, 5, size=5)))
