import pymysql

connect = pymysql.connect(
    host="localhost",
    port=3306,
    user="root",
    password="123456",
    db="db_16"
)

cursor = connect.cursor()

cursor.execute("select * from live_data")
for i in cursor.fetchall():
    print(i)

# 更新1800
cursor.execute("update live_data set 业务方向 = '热门' where 观看人数 > 1800 ")
connect.commit()
# 删除
cursor.execute("delete * from  live_data  where 评论数 < 30 or 评论数 is null  ")
connect.commit()

# 模糊查询
cursor.execute("select * from live_data where 直播标题 like '%美%' ")
for i in cursor.fetchall():
    print(i)
