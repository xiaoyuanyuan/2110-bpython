class LiveShow():
    def __init__(self, bt, xm, rs, dz, pl, fx):
        self.bt = bt
        self.xm = xm
        self.rs = rs
        self.dz = dz
        self.pl = pl
        self.fx = fx

    # 点赞数除以观看人数
    @staticmethod
    def get_rate(list):
        for l in list:
            print(l.dz / l.rs)

    # 观看人数最高的
    @staticmethod
    def cal_person_count(list):
        max = 0
        obj = None
        for l in list:
            if l.rs > max:
                max = l.rs
                obj = l
        print(obj)

    # 统计不用业务方向的直播分类
    @staticmethod
    def stat_biz_type(list):
        dict = {}
        for l in list:
            dict[l.bt] = ""
        print(dict.keys())

    # 判断是否上热门
    @staticmethod
    def get_hot(list):
        for l in list:
            if l.rs > 1500:
                print(l)
                # 写入文件

    @staticmethod
    def save(list):
        for l in list:
            if l.rs > 1800:
                with open("popular_live.txt", "a", encoding="utf-8") as f:
                    f.write(str(l))

    def __str__(self):
        return str(self.dz) + str(self.fx) + str(self.bt)
