import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from KS1 import LiveShow

# 读取文件
df = pd.read_csv("直播数据集.csv", encoding="gbk")
print(df)

# # 评价观看时长
# sc = df["观看时长（小时）"].mean()
# print(sc)

# 删除没有主播姓名,并统计业务方向的付费意愿
df.dropna(axis=0, inplace=True)
# print(df["广告收入"].sum())
# print(df["广告收入"] + df["礼物收入"])
df["付费意愿"] = (df["广告收入"] + df["礼物收入"]) / df["观看人数"]


# # 前3行，并统计点赞
df1 = df.head(3)
df["点赞->评论"] = df1["点赞数"] / df1["评论数"]
df["评论->分享"] = df1["评论数"] / df1["分享数"]
print(df)
#
# # 使用中位数,填充评论数
df["评论数"].fillna(df["评论数"].median(), inplace=True)

print(df["分享数"].mode())
# # 出现频率最高的，填充到分享数
df["分享数"].fillna(df["分享数"].mode(), inplace=True)
#
# # ---------------------------------------------------------------------------
#
lists = []
for i in df.index:
    lists.append(LiveShow(df.loc[i, "直播标题"],
                          df.loc[i, "主播姓名"],
                          df.loc[i, "观看人数"],
                          df.loc[i, "点赞数"],
                          df.loc[i, "评论数"],
                          df.loc[i, "分享数"]))
# get_date
LiveShow.get_rate(lists)
# cal_person_count
LiveShow.cal_person_count(lists)
# stat_biz_type
LiveShow.stat_biz_type(lists)
# get_hot
LiveShow.get_hot(lists)
# save
LiveShow.save(lists)

# 计算礼物收入总和
print(df["礼物收入"].sum())
# 排序
df.sort_values(by="观看人数", inplace=True, ascending=True)
# 直播的总输入
print(df["礼物收入"].sum() + df["广告收入"].sum())
