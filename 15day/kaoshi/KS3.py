import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# 读取文件
df = pd.read_csv("直播数据集.csv", encoding="gbk")

# 时间和观看人数趋势排序
# plt.figure(figsize=(10, 0))
# plt.plot(df["时间"], df["观看人数"])
# plt.xticks(rotation=35)
# plt.show()
#
# # 主播姓名和平均点赞数
# mean = df.groupby("主播姓名")["点赞数"].mean()
# plt.bar(mean.index, mean)
# plt.xticks(rotation=35)
# plt.show()
#
# # 礼物收入和广告收入
# sum1 = df["礼物收入"].sum()
# sum2 = df["广告收入"].sum()
# data = np.array([sum1, sum2])
# plt.pie(data, autopct="%.2f%%")
# plt.legend(["礼物收入", "广告收入"])
# plt.show()
# # 观看人数和点赞数
# plt.scatter(df["观看人数"], df["点赞数"])
# plt.show()

# 业务方向和直播时长
df["业务方向"] = df["直播标题"].str[0:2]
data = []
names = [0, ]
for index in df.index:
    data.append(df.loc(index, "直播时长(分钟)"))
    names.append(df.loc(index, "业务方向"))
plt.boxplot(data)
plt.xticks(np.arange(len(names), names))
plt.show()
