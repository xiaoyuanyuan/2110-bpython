#%%
import os
from datetime import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
plt.rcParams['font.sans-serif']=['SimHei']
plt.rcParams['axes.unicode_minus']=False

ratings_df = pd.read_csv("./studiodata/ratings.dat",sep='::',engine='python', names=['user_id','studio_id','rating','ts'])
print(ratings_df)