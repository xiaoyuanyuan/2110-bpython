"""
pip install scikit-learn -i https://pypi.tuna.tsinghua.edu.cn/simple


"""
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.metrics import accuracy_score
# X = np.array([[1, 1], [1, 2], [2, 2], [2, 3]])
# y = np.dot(X, np.array([1, 2])) + 3
# reg = LinearRegression().fit(X, y)
#
# # 回归系数
# print(reg.coef_)
# # 截距
# print(reg.intercept_)
#
# # 预测
# X_test = [[3, 5]]
# y_test = [16]
# # 预测
# y_pred = reg.predict(X_test)
# # 评估
# # 均方误差
# print(mean_squared_error(y_test, y_pred))

# 岭回归
from sklearn.linear_model import Ridge
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_iris

# # 分成训练集和测试集
X, y = load_iris(return_X_y=True)
# # 训练集80% 测试集20%
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
# # 创建岭回归模型
# # alpha控制正则化的强度
# clf = Ridge(alpha=1.0)
# # 训练
# clf.fit(X_train,y_train)
# print(f'回归系数:{clf.coef_}')
# print(f'截距:{clf.intercept_}')
# # 预测
# y_pred = clf.predict(X_test)
# # 评估y_pred和y_test
# # 均方误差
# print(mean_squared_error(y_test,y_pred))

#Lasso 套索回归
from sklearn.linear_model import Lasso

clf = Lasso(alpha=0.1)
clf.fit(X_train,y_train)
print(f'回归系数:{clf.coef_}')
print(f'截距:{clf.intercept_}')
# 预测
y_pred = clf.predict(X_test)
# 评估y_pred和y_test
# 均方误差
print(mean_squared_error(y_test,y_pred))
