#  倒三角形
# 创建一个二维列表，但这不是直接打印的方法
triangle_list = [
    ['*' if i <= 7 - j - 1 else ' ' for i in range(5)]  # i 控制每行的字符数，j 控制行数
    for j in range(7)
]

# 查看生成的二维列表（可选）
for row in triangle_list:
    print(''.join(row))

"""
*****
****
***
**
*
"""

# for i in range(7, 0,-1):
#     for j in range(2, i):
#         print("*", end="")
#     print("")

"""
    *  *  *  *  *
     *   *  *  *
       *   *  *
         *   *
           *
"""