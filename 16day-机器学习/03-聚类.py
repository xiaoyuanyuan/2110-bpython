#K-均值聚类

from sklearn.cluster import KMeans
import numpy as np
from sklearn.metrics import calinski_harabasz_score
X = np.array([[1, 2], [1, 4], [1, 0],
           [10, 2], [10, 4], [10, 0]])

kmeans = KMeans(n_clusters=2, random_state=0).fit(X)
print(kmeans.labels_)
lables = kmeans.predict(X)
print(calinski_harabasz_score(X ,lables))