# 逻辑回归 决策树/随机森林 knn

# 逻辑回归
from sklearn.datasets import load_iris
from sklearn.linear_model import LogisticRegression
import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error

# df = pd.read_csv("iris.data", names=['f0', 'f1', 'f2', 'f3', 'label'])
# print(df)
# # 选择特征
# X = df[['f0', 'f1', 'f2', 'f3']]
# y = df['label']
# # 分成训练集和测试集
# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)
# # 创建逻辑回归模型
# clf = LogisticRegression(random_state=0).fit(X_train, y_train)
# # 预测
# y_pred = clf.predict(X_test)
# # 分类模型的准确度
# print(accuracy_score(y_test, y_pred))

# 决策树
# from sklearn.tree import DecisionTreeClassifier
#
# df = pd.read_csv("iris.data", names=['f0', 'f1', 'f2', 'f3', 'label'])
# print(df)
# # 选择特征
# X = df[['f0', 'f1', 'f2', 'f3']]
# y = df['label']
# # 分成训练集和测试集
# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)
# # 创建逻辑决策树模型
# clf = DecisionTreeClassifier(random_state=0).fit(X_train, y_train)
# # 预测
# y_pred = clf.predict(X_test)
# # 分类模型的准确度
# print(accuracy_score(y_test, y_pred))

# 随机森林

# from sklearn.ensemble import RandomForestClassifier
# #
# df = pd.read_csv("iris.data", names=['f0', 'f1', 'f2', 'f3', 'label'])
# print(df)
# # 选择特征
# X = df[['f0', 'f1', 'f2', 'f3']]
# y = df['label']
# # 分成训练集和测试集
# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)
# # 创建逻辑决策树模型
# clf = RandomForestClassifier(max_depth=2, random_state=0).fit(X_train, y_train)
# # 预测
# y_pred = clf.predict(X_test)
# # 分类模型的准确度
# print(accuracy_score(y_test, y_pred))

# KNN
from sklearn.neighbors import KNeighborsClassifier
df = pd.read_csv("iris.data", names=['f0', 'f1', 'f2', 'f3', 'label'])
print(df)
# 选择特征
X = df[['f0', 'f1', 'f2', 'f3']]
y = df['label']
# 分成训练集和测试集
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)
# 创建逻辑决策树模型
clf = KNeighborsClassifier(n_neighbors=3).fit(X_train, y_train)
# 预测
y_pred = clf.predict(X_test)
# 分类模型的准确度
print(accuracy_score(y_test, y_pred))


