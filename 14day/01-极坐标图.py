import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# 让图片中可以显示中文
plt.rcParams['font.sans-serif'] = 'SimHei'
# 让图片中可以显示负号
plt.rcParams['axes.unicode_minus'] = False
fig = plt.figure(figsize=(8, 5))

N = 8  # 分成8份
x = np.linspace(0,  2*np.pi, N, endpoint=False)   # 360度等分成8份
height = np.random.randint(3, 15, size=N)  # 值
width = np.pi / 4  # 宽度
colors = np.random.rand(8, 3)  # 随机颜色
# polar表示极坐标
ax = plt.subplot(111,  projection='polar')
ax.bar(x=x, height=height, width=width, bottom=0, color=colors)




plt.show()