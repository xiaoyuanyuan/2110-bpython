from mpl_toolkits.mplot3d.axes3d import Axes3D
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

fig = plt.figure(figsize=(8, 5))

x = np.linspace(0, 100, 400)
y = np.sin(x)
z = np.cos(x)

axes = Axes3D(fig, auto_add_to_figure=False)
fig.add_axes(axes)
axes.plot(x, y, z)
plt.show()
