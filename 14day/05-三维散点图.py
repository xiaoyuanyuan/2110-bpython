from mpl_toolkits.mplot3d.axes3d import Axes3D
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

fig = plt.figure(figsize=(8, 5))
# 三维散点图
x = np.random.rand(50)
y = np.random.rand(50)
z = np.random.rand(50)

axes = Axes3D(fig, auto_add_to_figure=False)
fig.add_axes(axes)
axes.scatter(x, y, z)
plt.show()
