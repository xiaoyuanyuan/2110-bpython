import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# 让图片中可以显示中文
plt.rcParams['font.sans-serif'] = 'SimHei'
# 让图片中可以显示负号
plt.rcParams['axes.unicode_minus'] = False
fig = plt.figure(figsize=(8, 5))

x = np.linspace(-5, 5, 100)
y = np.linspace(-5, 5, 100)

# 将上述数据变成网格数据形式
X, Y = np.meshgrid(x, y)


# 定义Z与X, Y之间的关系
Z = np.sqrt(X**2 + Y**2)

# 画等高线
cp = plt.contourf(X, Y, Z)
# 颜色柱
plt.colorbar(cp)
plt.show()