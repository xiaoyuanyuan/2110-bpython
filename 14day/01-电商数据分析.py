import os
from datetime import datetime
import numpy as np
import pandas as pd
from pandas import Series,DataFrame
import matplotlib.pyplot as plt

# 让图片中可以显示中文
plt.rcParams['font.sans-serif'] = 'SimHei'
# 让图片中可以显示负号
plt.rcParams['axes.unicode_minus'] = False
df = pd.read_csv("1.csv", encoding="UTF-8", index_col=0)
df['event_time'] = pd.to_datetime(df['event_time'].str[:19], format="%Y-%m-%d %H:%M:%S")
# def fun(x):
#     d = x["event_time"]-x["event_time"].shift()
#     df = DataFrame(d)
#     print(df.dropna())
# # df1 = DataFrame(df[df["price"]>0].groupby("user_id").apply(lambda x:x["event_time"]-x["event_time"].shift())).dropna()
# # print(df1.describe())
# data = df[df["price"]>0].groupby("user_id").apply(fun)
# print(data.groups)
# print(data[data>0])

pivoted_amount =df[df['price']>0].pivot_table(index='user_id',
columns='Month',
values='price',
aggfunc='mean').fillna(0)
print(pivoted_amount)
