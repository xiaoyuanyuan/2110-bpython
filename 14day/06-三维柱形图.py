from mpl_toolkits.mplot3d.axes3d import Axes3D
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

fig = plt.figure(figsize=(8, 5))

# 二维变成了三维
axes = Axes3D(fig, auto_add_to_figure=False)
fig.add_axes(axes)

x = np.arange(1, 5)


for m in x:
    axes.bar(
        np.arange(4),
        np.random.randint(10, 100, size=4),
        zs=m,  # 在x轴中的第几个
        zdir='x',  # 在哪个方向上排列
        alpha=0.7,  # 透明度
        width=0.5  # 宽度
    )

axes.set_xlabel('X轴', fontsize=18, color='red')
axes.set_ylabel('Y轴', fontsize=18, color='blue')
axes.set_zlabel('Z轴', fontsize=18, color='green')
plt.show()
