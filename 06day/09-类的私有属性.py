class Dog():
    __instance = None  # 类的私有属性 内部能用，外部不能用
    num = 0  # 类属性

    def test(self):
        print(Dog.__instance)  # 内部能调用


print(Dog.num)
# print(Dog.__instance) # 外部不能调用
