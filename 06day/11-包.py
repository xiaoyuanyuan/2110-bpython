# 包 多个模块放在一个文件夹下，并且该文件夹用__init__.py文件
# from msg import send,receive
# send.send_msg()
# receive.receive()

# import msg  # 需要包下的__init__文件手动导入模块
#
# msg.send.send_msg()
# msg.receive.receive()

# from msg import *
# send.send_msg()
