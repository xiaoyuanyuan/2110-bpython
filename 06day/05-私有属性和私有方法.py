class Father():
    def __init__(self, name):
        self.name = name  # 非常普通实例属性
        self.__money = 10000  #

        # -------
        self.a = 1  # 普通属性
        self.__b = 10  # 私有属性
        self._c = 100  # 没有任何的约束力
        self.int_ = 10  # 跟关键字解决冲突

    # https://blog.csdn.net/u014612521/article/details/122298168
    def __make_money(self):  # 私有方法
        print("会赚钱")


class Son(Father):
    pass


s = Son("小头儿子")
print(s.name)
# print(s.__money)
print(s.__dir__())  # 查看一些属性
# print(s._Father__money) # 不建议
# s._Father__make_money() # 不建议
