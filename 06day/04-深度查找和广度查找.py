class A():
    def a(self):
        print("a1")


class B(A):
    # def a(self):
    #     print("a2")
    pass


class C(A):
    # def a(self):
    #     print("a3")
    pass


class D(B, C):
    # def a(self):
    #     print("a4")
    pass


d = D()
d.a()
print(D.__mro__)

# 在py3当中，广度优先
# 在py2当中，深度优先
