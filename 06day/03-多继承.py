#  Java 单继承 逻辑清晰
#  Python 多继承

class Uncle():
    def __init__(self):
        print("1")
        self.weight = 100

    def eat(self):
        print("会吃")


class Mother():
    def __init__(self):
        print("2")
        self.facevalue = 100

    def cookie(self):
        print("会做饭")


class Father():
    def __init__(self):
        print("3")
        self.height = 170

    def paly(self):
        print("会运动")


# 儿子
class Son(Father, Mother, Uncle):
    def __init__(self):
        print("4")
        # 手动执行
        Father.__init__(self)
        Mother.__init__(self)
        Uncle.__init__(self)


s = Son()  # 只会执行第一个父类的init方法
# print(Son.__mro__) # 打印继承顺序的一个属性
print(s.height)
print(s.weight)
print(s.facevalue)

s.paly()
s.eat()
s.cookie()
