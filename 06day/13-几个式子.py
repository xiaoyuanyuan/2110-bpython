# 列表推导式
# 生成0-100列表
l = []
for i in range(0, 101):
    l.append(i)
print(l)
# 列表推导式
l1 = [i for i in range(0, 101)]
print(l1)
# 简单判断
l2 = [i for i in range(0, 101) if i % 2 == 0]
print(l2)
# 嵌套循环
l3 = [[i, j, k] for i in range(5) for j in range(5) for k in range(5)]
print(l3)

#  字典推导式
# s = "我爱中国"
# d = {i: ord(i) for i in s}
# print(d)
# k:v ---> v:k
d = {"a": 1, "b": 2}
d1 = {}
for k, v in d.items():
    d1[v] = k
print(d1)
d2 = {v: k for k, v in d.items()}
print(d2)

# 集合推导式
s3 = {i for i in range(10)}
print(s3)
