# 小b功能
def add(a, b):
    return a + b


def isum(a, b):
    sum = 0
    for i in range(a, b + 1):
        sum += i

    return sum


if __name__ == '__main__':
    print(add(1, 2))
    print(isum(1, 10))
