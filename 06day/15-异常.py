# try:
#     print(1 / 0)
#     f = open("1.txt","r")
# except Exception as e:
#     print(e)

# try:
#     # print(1 / 0)
#     f = open("1.txt","r")
# except (ZeroDivisionError,FileNotFoundError) as e:
#     print(e)

# 自定义异常
# 假如你输入老王，就报异常
class MyException(Exception):
    def __init__(self, msg):
        self.msg = msg


name = input("请输入名字")
try:
    if name == "老王":
        raise MyException("不能输入老王")
except MyException as e:
    print(e.msg)
