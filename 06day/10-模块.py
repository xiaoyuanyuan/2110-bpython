# 模块 单个python文件叫模块
# import util
# print(util.add(1,2))
# print(util.isum(1,100))

from util import add, isum
# print(add(1,2))

# from util import *
# from common import *
# 【注意】：避免后面导入的方法覆盖前面导入的方法

# print(add(1, 2))
# print(isum(1, 2))

# import myrandom
# print(myrandom.MyRandom().create_random(10))
from myrandom import MyRandom

# from myrandom import *
print(MyRandom().create_random(10))
