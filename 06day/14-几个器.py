# 装饰器
# 生成器 生成数据
g = (i for i in range(3))
print(g)
# print(g.__next__())
# print(g.__next__())
# print(g.__next__())
# print(g.__next__())
# print(g.__next__())
# print(next(g))
# print(next(g))
# 迭代器 能用next方法
#  生成器一定是迭代器
#  能迭代的对象不一定是迭代器
from collections.abc import Iterator

l = []
print(isinstance(l, Iterator))

# 把迭代的对象变成迭代器
print(isinstance(iter(l), Iterator))

l = [1,2,3,4]
l1 = iter(l)
print(l1.__next__())
print(l1.__next__())
print(l1.__next__())
