import threading

a = 0

# 创建一把锁
lock = threading.Lock()


def test1(*args):
    print(args)
    global a
    for i in range(1000000):
        if lock.acquire():
            a += 1
        lock.release()


def test2():
    global a
    for i in range(1000000):
        if lock.acquire():
            a += 1
        lock.release()


t1 = threading.Thread(target=test1, args=(1, 2))
t2 = threading.Thread(target=test2)
t1.start()
t2.start()
# 让主线程等着子线程运行完毕
t1.join()
t2.join()
print(a)
