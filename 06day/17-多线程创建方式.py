import threading


# print(threading.currentThread())

def test1():
    for i in range(10):
        print("test1-" + str(i))


def test2():
    for i in range(10):
        print("test2-" + str(i))


t1 = threading.Thread(target=test1)
t2 = threading.Thread(target=test2)
t1.start()
t2.start()

"""
    并发：谁抢到CPU执行权，谁执行
    并行：多个任务真正一起执行
"""
