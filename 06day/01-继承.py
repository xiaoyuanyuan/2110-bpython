# 继承:子类继承父类，实现代码复用


# 父类
class Animal():
    def __init__(self, name):
        self.name = name

    def sleep(self):
        print("睡觉")


class Dog(Animal):
    pass


class Pig(Animal):
    pass


dog = Dog("旺财")
pig = Pig("佩奇")
print(dog.name)
dog.sleep()
print(pig.name)
pig.sleep()
