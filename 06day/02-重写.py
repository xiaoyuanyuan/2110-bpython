# Java 重写 重载
# 当父类满足不了子类需求时候，需要重写
# 继承:子类继承父类，实现代码复用


# 父类
class Animal():
    def __init__(self, name):
        self.name = name

    def sleep(self):
        print("睡觉")


class Dog(Animal):


    def sleep(self):
        print("小狗趴着睡")


class Pig(Animal):
    def sleep(self):
        print("小猪侧着睡")


dog = Dog("旺财")
pig = Pig("佩奇")
print(dog.name)
dog.sleep()  # 当子类有自己的方法，会优先调用自己的
print(pig.name)
pig.sleep()
