from threading import Thread


class MyThread(Thread):

    def __init__(self, name):
        super().__init__()  # 调用父类的init
        self.name = name

    def run(self):
        for i in range(100):
            print(self.name + "test-" + str(i))


for i in range(3):
    t1 = MyThread(i)
    t1.start()
