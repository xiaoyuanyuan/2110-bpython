# class Dog():
#     @property
#     def paly(self):
#         print("跑")
#
#
# dog = Dog()
# dog.paly  # 希望像调用属性一样去调用方法


# 应用场景

class Person():
    def __init__(self):
        self.__age = 0  # 私有属性

    # 对外公开的方法，去操作私有属性
    # def set(self, age):
    #     self.__age = age
    #
    # def get(self):
    #     return self.__age

    @property
    def age(self):
        return self.__age

    @age.setter
    def age(self, age):
        self.__age = age


p = Person()
# p.__age = 10  # 普通变量，普通变量的名字跟私有属性的变量名字一样而已
# print(p.__age)
# p.set(10)  # 调用方法，打印私钥属性的值
# print(p.get())  # 调用方法，打印私钥属性的值

p.age = 10  # 这么写相当于set
print(p.age)  # 这么写相当于get
