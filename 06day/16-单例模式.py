# 23设计模式  单例模式 工厂模式 建造者模式  适配器模式..
# 不管创建多少个对象，内存中只有一份


class Dog(object):
    __instance = None  # 类的私有属性

    # 重写new
    def __new__(cls, *args, **kwargs):  # 创建对象
        if not Dog.__instance:  # 如果没有__instance==None 创建对象
            # 把创建好的对象的引用赋值给__instance
            Dog.__instance = object.__new__(cls, **kwargs)
        return Dog.__instance

    # 属性赋值
    def __init__(self, name):
        self.name = name


dog = Dog("旺财")
# print(dog)
print(id(dog))
dog1 = Dog("旺财")
# print(dog1)
print(id(dog1))

dog2 = Dog("旺财")
print(id(dog2))
print(dog.name)
