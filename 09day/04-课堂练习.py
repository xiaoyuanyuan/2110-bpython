import numpy as np
import pandas as pd
from pandas import Series

# 通过官方提供
s1 = Series(data=[150, 150, 150, 300], index=["语文", "数学", "英语", "理综"])
print(s1)
# 通过字典创建
d = {
    "语文": 150,
    "英语": 150,
    "数学": 150,
    "理综": 150,
}
s2 = Series(d)
print(s2)
