import numpy as np
import pandas as pd
from pandas import Series

data = [150, 150, 150, 150, 150]
s = Series(data=data, index=["tom", "mery", "ben", "jack", "tony"])
#  切片
#  使用显示索引切片都是闭区间，使用隐式索引切片都是开区间
print(s)

# 显示索引
print(s["tom":"mery"])
print(s.loc["tom":"mery"])

# 隐式索引
print(s[0:2])
print(s.iloc[0:2])

# 课堂练习
s_score = Series(data=[150, 150, 150, 300], index=["语文", "数学", "英语", "理综"])

# 通过索引访问 数学150
print(s_score[1])
print(s_score["数学"])
print(s_score.loc["数学"])
print(s_score.iloc[1])

# 切片
print(s_score[0:3])
print(s_score["语文":"英语"])
print(s_score.loc["语文":"英语"])
print(s_score.iloc[0:3])
