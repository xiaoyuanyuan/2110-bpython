import pandas as pd
import numpy as np
from pandas import Series, DataFrame

dict = {
    "lucy": 80,
    "merry": 90,
    "jack": 100
}
s1 = Series(dict, index=["lucy", "merry", "jack", "ben"])
print(s1)
print(s1.isnull())  # 查看是否为空
print(s1.notnull())  # 是否不为空
# any() all()
print(s1.isnull().any())  # 返回True 证明里面有空值
print(s1.notnull().all())  # 返回False 证明里面有空值

s2 = Series(dict, index=["lucy", "merry", "jack", "ben"], name="score")
print(s2)
