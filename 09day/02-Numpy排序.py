import numpy as np

arr1 = np.random.permutation(10)
print(arr1)

print(arr1.sort())  # 没有返回值  升序
# # 没有返回值
print(arr1)
print(arr1[::-1])  # 降序

print(np.sort(arr1)[::-1])  # 在元数据做改变
print(arr1)

data = np.random.permutation(100000)
print(data)
# - 当k为正时，我们想要得到最小的k个数
# - 当k为负时，我们想要得到最大的k个数
print(np.partition(data, 5))
print(np.partition(data, 5)[:5])
print(np.partition(data, -5))
