import numpy as np

arr1 = np.array([[1, 2], [3, 4]])
arr2 = np.array([[1, 2], [3, 4]])
print(arr1)
print(arr2)

# 相同维度 numpy和numpy的运算
# print(arr1 + arr2)
# print(arr1 - arr2)
# print(arr1 * arr2)
# print((arr1 / arr2).dtype)

# numpy和一个数字  广播机制
print(arr2 > 2)
print(arr2 + 2)

# 不同维度，numpy和numpy运算
# arr3 = np.array([[1, 2], [3, 4]])
# # arr4 = np.array([[2, 2, 2], [2, 2, 2]])
# arr4 = np.array([[2, 2, 2], [2, 2, 2]])
# # 小+大，已小的为准
# print(arr3 + arr4)
# # 大+小
# print(arr4 + arr4)

# 不同维度，numpy和numpy运算
# arr3 = np.array([[1, 2], [3, 4]])
# arr4 = np.array([[2, 2], [2, 2],[2, 2],[2, 2]])
# #  小+大
# print(arr4 + arr3)


import numpy as np

# 创建两个矩阵
matrix1 = np.array([[1, 2, 3],
                    [4, 5, 6],
                    [7, 8, 9]])

matrix2 = np.array([[1],
                    [2],
                    [3]])

# 将两个矩阵相加
result = matrix1 + matrix2

print("相加后的结果：")
print(result)

#
# m = np.ones(shape=(3,3))
# n = np.array([[1,2],[3,4]])
# print(m+n)


# 两个都一起补
a = np.arange(0, 3, step=1).reshape((3, 1))
print(a)
b = np.arange(3)
print(b)
print(a + b)
