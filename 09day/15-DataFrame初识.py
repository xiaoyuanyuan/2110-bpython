from pandas import Series, DataFrame
import numpy as np
import pandas as pd

# DataFrame 二维
#  行索引 index
#  列索引 columns

d = {
    "name": Series(data=["A", "B", "C", "D"], index=list('ABCD')),
    "age": Series(data=[20, 22, 23, 25], index=list('ABCD')),
    "salary": Series(data=[1000, 221, 21, 2115], index=list('ABCD')),
}

# 使用构造函数来创建
# df = DataFrame(data=np.random.randint(0, 100, size=(3, 5)), index=list("abc"), columns=list("12345"))
# df = DataFrame(data=np.random.randint(0, 100, size=(3, 5)))
# print(df)


# 通过字典方式创建
# d = {
#     "name": Series(data=["A", "B", "C", "D"], index=list('ABCD')),
#     "age": Series(data=[20, 22, 23, 25], index=list('ABCD')),
#     "salary": Series(data=[1000.2, 221, 21, 2115], index=list('ABCD')),
# }

d = {
    "name": Series(data=["A", "B", "C", "D"]),
    "age": Series(data=[20, 22, 23, 25]),
    "salary": Series(data=[1000.2, 221, 21, 2115]),
}
# df1 = DataFrame(data=d, index=[0, 1, 2, 3])
# df1 = DataFrame(data=d, index=list("abcd"))  # 不行
# print(df1)


dic = {
    "A": ["lucy", "merry", "tom"],
    "B": np.random.randint(0, 100, size=3),
    "C": np.random.random(3) * 10000,
    "D": np.random.randint(0, 100, size=3),
    "E": np.random.randint(0, 100, size=3),
}
df2 = DataFrame(data=dic, index=list("abc"))
print(df2)

# 从外部读取

# pip install xlrd -i https://pypi.tuna.tsinghua.edu.cn/simple

df3 = pd.read_excel("mydata.xls")
print(df3)

