import pandas as pd
import numpy as np
from pandas import Series, DataFrame

s1 = Series(data=np.random.randint(1, 100, size=5), index=list("bacde"), name="score")
print(s1)
# print(s1.sort_values())  # 根据值排序，升序
# print(s1.sort_values(ascending=False))  # 根据值排序，降序

print(s1.sort_index())  # 根据索引升序
print(s1.sort_index(ascending=False))  # 根据索引降序

# 根据ascii码排序
s2 = Series(data=np.random.randint(1, 100, size=5), index=["小红", "赵刚", "李四", "王五", "王六"], name="score")
print(s2.sort_index())  # 根据索引升序
print(ord("小"))
print(ord("李"))
print(ord("王"))
print(ord("赵"))
