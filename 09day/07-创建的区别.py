import pandas as pd

import numpy as np
from pandas import Series, DataFrame

l = [1, 2, 3, 4, 5]
s1 = Series(l)  # 副本创建
print(s1)
s1[0] = 10
print(l)
print(s1)

arr = np.ones(shape=5)
s2 = Series(arr)  # 引用创建
print(s2)
s2[0] = 10
print(s2)
print(arr)
