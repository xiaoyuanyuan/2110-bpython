import numpy as np
import pandas as pd
from pandas import Series, DataFrame

s = Series(data=np.random.randint(0, 10, size=5), index=list("abcde"))
print(s)
# 返回形状
print(s.shape)
# 返回大小
print(s.size)
# 返回显示索引
print(s.index)
print(s.index[0])
# 返回值
print(s.values)
# numpy.ndarray
print(type(s.values))

# 显示索引做比较
s_index = s.index
print((s_index == "100").any())
print((s_index == "100").all())

s1 = Series(data=np.random.randint(0, 10, size=5), index=s_index)
print(s1)

