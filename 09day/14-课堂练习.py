import pandas as pd
import numpy as np
from pandas import Series, DataFrame

index = ["lucy", "merry", "tom", "jack"]
python = Series(data=np.random.randint(1, 100, size=4), index=index, name="python")
java = Series(data=np.random.randint(1, 100, size=4), index=index, name="java")
print(python)
# print(java)

# 求每个学生成绩
# print((python + java) / 2)

# 找出未及格，Series跟一个数字运算
# bool_list = python < 60
# print(python[bool_list])
# print(python[bool_list].index)
# print(python.loc[bool_list])  # 显示索引

# 找出Java未及格的学生
# bool_list = java < 60
# print(java[bool_list])
# print(java[bool_list].index)

# 5 给merry加10分
# python[1]+=10
# print(python)

# python["merry"]+=10
# print(python)

# python.loc["merry"]+=10
# print(python)

# python.iloc[1]+=10
# print(python)

# 求平均数
# arr1 = np.random.randint(0, 10, size=5)
# print(arr1.mean())
# print(arr1.sum())

print(python.values.mean())
# 或者直接mean也行
print(python.mean())
