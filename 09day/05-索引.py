import numpy as np
import pandas as pd
from pandas import Series

arr = np.random.randint(0, 100, size=5)
# print(arr)
# print(arr[0])
# print(arr[[0, 1, 1, 1]])
# print(arr[[True, False, True, False, False]])

data = [150, 150, 150, 150, 150]
s = Series(data=data, index=["tom", "mery", "ben", "jack", "tony"])
print(s)
# 支持numpy方法隐式索引方式
print(s[0])
print(s[[0, 1, 1, 1]])
print(s[[True, False, True, False, False]])
# 支持显示索引访问
print(s["tom"])
print(s[["tom", "tony"]])

# loc和iloc 官方推荐
# loc 用于显示索引方法
# iloc 用隐式索引方法
print(s.loc["tom"])
print(s.loc[["tom", "tony"]])
print(s.iloc[0])
print(s.iloc[[0, 1]])

# python列表
# s_bool = [True, True, False, False, False]
# print(s[s_bool])  #直接用python列表可以访问

# ndarry类型
# n_bool = np.array([True, True, False, False, False])
# print(s[n_bool])
# 【注意】bool列表和要访问的对象的长度保持一致


# Series类型
# s_bool = Series(data=[True, True, False, False, False])  # 这种不行，必须要索引对齐
# s_bool = Series(data=[True, True, False, False, False], index=["tom", "mery", "ben", "jack", "tony"])
# s_bool = Series(data=[True, True, False, False], index=["tom", "mery", "ben", "jack"]) # 这种不行

# 数据量一直，顺序可以不一致
# s_bool = Series(data=[True, True, False, False, False], index=["ben", "jack", "tom", "mery", "tony"])
# print(s[s_bool])
# print(s.loc[s_bool])


