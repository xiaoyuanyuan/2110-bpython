import pandas as pd
import numpy as np
from pandas import Series, DataFrame

#  Series 跟一个数 运算  根据广播机制来
# s1 = Series(data=[1, 2, 3, 4, 5])
# print(s1 + 2)
# print(s1 - 2)
# print(s1 / 2)
# print(s1 * 2)

#  Series 跟Numpy 运算    隐式索引对齐
# arr1 = np.random.randint(0, 2, size=5)
# print(arr1)
# print(s1 + arr1)

# 加不了
arr1 = np.random.randint(0, 2, size=(2, 5))
# print(arr1)
# print(s1 + arr1) # Series和ndarry 维度不一样，是加不了，也不支持广播
# 你可以这么做
# print(type(s1.values))
# print(s1.values + arr1)  # ndarry+ndarry ,支持广播运算

#  Series 跟Series 运算
# - 在运算中自动对齐不同索引的数据
# - 如果索引不对应，则补NaN
# s2 = Series(data=[1, 2, 3, 4, 5])
# s3 = Series(data=[1, 2, 3, 4, 5])
# print(s2 + s3)

# s2 = Series(data=[1, 2, 3, 4, 5], index=list("abcde"))
# s3 = Series(data=[1, 2, 3, 4, 5], index=list("xyzhj"))
# print(s2 + s3)


# 显示索引对齐
# s2 = Series(data=[1, 2, 3, 4, 5], index=list("abcde"))
# s3 = Series(data=[1, 2, 3, 4, 5], index=list("cbade"))
# print(s2 + s3)

s4 = Series(data=np.random.randint(1, 100, size=3), index=["老王", "老宋", "老李"])
s5 = Series(data=np.random.randint(1, 100, size=3), index=["老王", "老宋", "老张"])
print(s4 + s5)
print(s4.add(s5, fill_value=0))

# 想一想Series运算和ndarray运算的规则有什么不同？

# ndarray   ndarray 广播

# Series Series  显示索引对齐
# Series Numpy一维  隐式索引对齐
# Series Numpy二维  加不了
# Series.values Numpy二维  广播


index1 = ["语文", "数学", "英语", "理综"]
index2 = ["语文", "数学", "英语", "文综"]

score1 = Series(data=np.random.randint(0, 150, size=4), index=index1, name="理科")
score2 = Series(data=np.random.randint(0, 150, size=4), index=index2, name="文科")
print(score1)
print(score2)
print(score1 + score2)
print(score1.add(score2, fill_value=0))

#
score1["文综"] = 0
score2["理综"] = 0
print(score1 + score2)
