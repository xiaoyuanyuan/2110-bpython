import pandas as pd
import numpy as np
from pandas import Series, DataFrame

s1 = Series(data=np.random.randint(0, 10, size=500))
print(s1)
print(s1.head())  # 默认取前5
print(s1.tail())  # 默认取后5
print(s1.head(10))  #
print(s1.tail(10))  #
