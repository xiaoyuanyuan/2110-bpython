#  安装 pip install pandas -i https://pypi.tuna.tsinghua.edu.cn/simple

import numpy as np
import pandas as pd
from pandas import Series, DataFrame

# Series  数组加强版，它有显式索引，隐式索引，并且只是一维度

# 没有指定显式索引，那就拿隐式的索引的值填充显式索引的位置上
# 使用ndarray构造
arr = np.ones(shape=5)
s1 = Series(arr)
print(s1)
# 使用列表方式构造
names = ["A", "B", "C", "D"]
s2 = Series(names)
print(s2)

# 指定显示索引
names = ["A", "B", "C", "D"]
# s2 = Series(names,index=list("abcd"))
s2 = Series(names, index=["a", "b", "c", "d"])
print(s2)

# 根据字典创建
#  字典的key当做显式索引，字典的值当做值  字典无序的
d = {
    "k1": "A",
    "k2": "B",
    "k3": "C"
}
# s3 = Series(d)
# s3 = Series(d,index=list("abc"))
# s3 = Series(d,index=["k1","k2","k3"])
# index优先级要比字典的key要高
# s3 = Series(d,index=["k2","k3","k1"])
# s3 = Series(d,index=["k2","k3"])
# print(s3)
# d = {
#     "k1":["A","B","C"],
#     "k2":["A","B","C"],
#     "k3":["A","B","C"]
# }
# s4 = Series(d)
# print(s4)
