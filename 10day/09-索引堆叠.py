import numpy as np
import pandas as pd
from pandas import Series

data = np.random.randint(0, 100, size=(6, 6))
index = pd.MultiIndex.from_product(
    [
        ['1班', '2班'],
        ['张三', '李四', '王五']
    ]
)
columns = [
    ['期中', '期中', '期中', '期末', '期末', '期末'],
    ['语文', '数学', '英语', '语文', '数学', '英语']
]
df = pd.DataFrame(data=data, index=index, columns=columns)
# print(df)
# 把列索引变成行索引
# print(df.stack())
# print(df.stack(level=1))
# print(df.stack(level=-1))
# print(df.stack(level=0))


# 把行索引变成列索引
# print(df.unstack())
# print(df.unstack(level=0))


data = np.random.randint(0, 100, size=(6, 6))
index = pd.MultiIndex.from_tuples(
    (
        ('1班', '张三'), ('1班', '李四'), ('1班', '王五'),
        ('2班', '赵六'), ('2班', '田七'), ('2班', '孙八'),
    )
)
columns = [
    ['期中', '期中', '期中', '期末', '期末', '期末'],
    ['语文', '数学', '英语', '语文', '数学', '英语']
]
df2 = pd.DataFrame(data=data, index=index, columns=columns)
print(df2)

# todo
# print(df2.unstack())
# print(df2.unstack(fill_value=0))
