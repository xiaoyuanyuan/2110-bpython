import pandas as pd
import numpy as np
from pandas import Series, DataFrame

d = {
    "name": ["鲁班", "程咬金", "猪八戒"],
    "age": [7, 8, 9],
    "sex": ["男", "女", "男"],
}
df = DataFrame(d)
print(df)
print(df.values)  # 查看值
print(type(df.values))  # ndarray
print(df.columns)  # 查看列索引
print(df.index)  # 查看行索引
print(type(df.index))  # 查看类型
print(df.shape) #查看形状
print(df.head()) #默认前5行
print(df.tail()) #默认后5行


