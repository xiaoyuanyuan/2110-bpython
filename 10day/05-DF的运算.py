import pandas as pd
import numpy as np
from pandas import Series, DataFrame

# 创建DataFrame df1 不同人员的各科目成绩，月考一
df1 = DataFrame(
    data={
        'Python': [100, 90, 80],
        'Java': [80, 70, 60],
        'PHP': [60, 50, 40]
    },
    index=['张飞', "吕布", '关羽']
)

# 创建DataFrame df2 不同人员的各科目成绩，月考二
df2 = DataFrame(
    data={
        'Python': [100, 90, 80, 70],
        'Java': [80, 70, 60, 50],
        'PHP': [60, 50, 40, 30],
        'Go': [40, 30, 20, 10]
    },
    index=['张飞', "吕布", '关羽', "刘备"]
)

print(df1)
print(df2)

#  根据一个数字
# print(df1 + 10)

# DF和DF 运算  行对齐，列对齐
print(df1 + df2)

#  自动填充

print(df1.add(df2, fill_value=0))

# Series和DF之间运算


# 创建DataFrame df3 不同人员的各科目成绩，月考一
df3 = DataFrame(
    data={
        'Python': [100, 90, 80],
        'Java': [80, 70, 60],
        'PHP': [60, 50, 40]
    },
    index=['张飞', "吕布", '关羽']
)

# Series的显式索引用的DF的列索引
s = Series([100, 10, 1], index=df1.columns)
# s = Series([100, 10, 1], index=list("abc"))  # 找不到对应的列索引
print(s)
print(df3)
# print(df3 + s)
# print(df3.add(s))  # 跟df3 + s一样
#
# # columns和1的效果是一样 列加
# print(df3.add(s, axis="columns"))
# print(df3.add(s, axis=1))


s = Series([100, 10, 1], index=df3.index)
print(s)
# # index和0的效果是一样 行加
print(df3.add(s, axis="index"))
print(df3.add(s, axis=0))

#

df4 = DataFrame(
    data={
        'Python': [100, 90, 80],
        'Java': [80, 70, 60],
        'PHP': [60, 50, 40]
    },
    index=['张飞', "吕布", '关羽']
)
arr = np.array([1, 2, 3])
# print(df4 + arr)
print(df4)
# print(df4.add(arr, axis=0))
print(df4.add(arr, axis=1))
