import pandas as pd
import numpy as np
from pandas import Series, DataFrame

data = np.random.randint(0, 100, size=6)
index = [
    ['1班', '1班', '1班', '2班', '2班', '2班'],
    ['张三', '李四', '王五', '赵六', '田七', '孙八']
]

s = pd.Series(data=data, index=index)
print(s)

# 显示索引
# print(s["1班"])
# print(s.loc["1班"])
# print(s.loc[["1班", "2班"]])
# print(s["1班"]["张三"])
# print(s["1班", "张三"])
# print(s.loc["1班"]["张三"])
# print(s.loc["1班", "张三"])

# 隐式索引
# print(s[1])
# print(s.iloc[1])
# print(s.iloc[[3, 2]])

# 切片  显示 闭区间
# print(s["1班":"1班"])
# print(s.loc["1班":"1班"])
# print(s.loc["李四":"田七"]) #没有结果
# print(s.loc[("1班", "王五"):("2班", "田七")])  #报错  显式切片只对最外层索引有效

# 隐式
print(s[1:5])
print(s.iloc[1:5])
