import pandas as pd
import numpy as np
from pandas import Series, DataFrame

data = np.random.randint(0, 100, size=(6, 6))

index = pd.MultiIndex.from_product(
    [
        ['1班', '2班'],
        ['张三', '李四', '王五']
    ]
)
columns = [
    ['期中', '期中', '期中', '期末', '期末', '期末'],
    ['语文', '数学', '英语', '语文', '数学', '英语']
]
df = pd.DataFrame(data=data, index=index, columns=columns)
print(df)
# 获取索引
# print(df["期中"]["数学"][1])
# 用隐式索引 1行3列
# print(df.iloc[1, 3])

# 列索引
# print(df["期中"])
# print(df["期中"]["数学"])
# print(df["期中", "数学"])
# print(df[("期中", "数学")])
# print(df.期中.数学)
# 隐式索引
# print(df.iloc[:, 2])
# 行全要，列要 1 2 3列
# print(df.iloc[:, [1, 2, 3]])

# 行索引  行必须要loc
# print(df.loc["1班"])
# print(df.loc["1班"].loc["张三"])
# print(df.loc["1班", "张三"])
# print(df.loc[("1班", "张三")])
# 隐式索引
# print(df.iloc[1])
# print(df.iloc[1])
# print(df.iloc[[1, 2, 3]])

# --------------------- 行切片
# print(df.iloc[1:3])
# print(df.loc["1班":"2班"])
# print(df.loc["1班":"2班"])
# DF可以切，Series不行
# print(df.loc[("1班", "李四"):("2班", "李四")])

# ------------------列切片
print(df.iloc[:, 1:4])
print(df.loc[:, "期中":"期末"])
print(df.loc[:, ("期中", "数学"):("期末", "数学")])  # 报错
