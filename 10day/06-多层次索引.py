import pandas as pd
import numpy as np
from pandas import Series, DataFrame

data = np.random.randint(0, 100, size=(6, 6))
# index = [["1班", "1班", "1班", "2班", "2班", "2班"]
#     , ["张三", "李四", "王五", "赵六", "田七", "孙八"]]
#
# columns = [["期中", "期中", "期中", "期末", "期末", "期末"]
#     , ["语文", "数学", "英语", "语文", "数学", "英语"]]
#
# df = DataFrame(data=data, index=index, columns=columns)
# print(df)
#
# data = np.random.randint(0, 100, size=6)
#
# index = [
#     ['1班', '1班', '1班', '2班', '2班', '2班'],
#     ['张三', '李四', '王五', '赵六', '田七', '孙八']
# ]
# s = Series(data=data, index=index)
# print(s)


#  通过from_arrays
# index = pd.MultiIndex.from_arrays([["1班", "1班", "1班", "2班", "2班", "2班"]
#                                       , ["张三", "李四", "王五", "赵六", "田七", "孙八"]])
# columns = [["期中", "期中", "期中", "期末", "期末", "期末"]
#     , ["语文", "数学", "英语", "语文", "数学", "英语"]]
#
# df1 = DataFrame(data=data, index=index, columns=columns)
# print(df1)


# 通过元组
# index = pd.MultiIndex.from_tuples(( ('1班', '张三'), ('1班', '李四'), ('1班', '王五'),
#         ('2班', '赵六'), ('2班', '田七'), ('2班', '孙八'), ))
#
# columns = [["期中", "期中", "期中", "期末", "期末", "期末"]
#     , ["语文", "数学", "英语", "语文", "数学", "英语"]]
# df1 = DataFrame(data=data, index=index, columns=columns)
# print(df1)

#

index = pd.MultiIndex.from_product([["1班", "2班"], ['张三', '李四', '王五']])

columns = [["期中", "期中", "期中", "期末", "期末", "期末"]
    , ["语文", "数学", "英语", "语文", "数学", "英语"]]
df1 = DataFrame(data=data, index=index, columns=columns)
print(df1)
