import numpy as np
import pandas as pd
from pandas import Series

# # 多对一的情况
# df1 = pd.DataFrame({
#     'name': ['张三', '李四', '王五'],
#     'id': [1, 2, 2],
#     'age': [22, 33, 44]
# })
# df2 = pd.DataFrame({
#     'id': [2, 3, 4],
#     'sex': ['男', '女', '女'],
#     'job': ['saler', 'CTO', 'Programer']
# })
# print(df1)
# print(df2)
# print(df1.merge(df2))

# 多对多

# df1 = pd.DataFrame({
#     'name': ['张三', '李四', '王五'],
#     'id': [1, 2, 2],
#     'age': [22, 33, 44]
# })
# df2 = pd.DataFrame({
#     'id': [2, 2, 4],
#     'sex': ['男', '女', '女'],
#     'job': ['saler', 'CTO', 'Programer']
# })
# print(df1)
# print(df2)
# print(df1.merge(df2))


# 两个列一样
# df1 = pd.DataFrame({
#     'name': ['张三', '李四', '王五'],
#     'id': [1, 2, 3],
#     'age': [22, 33, 44]
# })
# df2 = pd.DataFrame({
#     'id': [2, 3, 4],
#     'name': ['李四', '王五', '赵六'],
#     'job': ['saler', 'CTO', 'Programer']
# })
# print(df1)
# print(df2)
# print(df1.merge(df2))

# 指定某个列 如果有多列名称相同， 则需要指定一列作为连接的字段
# print(df1.merge(df2,on="id"))
# print(df1.merge(df2,on="name"))


# 如果没有相同的列名，则需要使用left_on和right_on来分别指定2个表的列作为连接的字段

# df1 = pd.DataFrame({
#     'name1': ['张三', '李四', '王五'],
#     'id1': [1, 2, 3],
#     'age': [22, 33, 44]
# })
# df2 = pd.DataFrame({
#     'id2': [2, 3, 4],
#     'name2': ['李四', '王五', '赵六'],
#     'job': ['saler', 'CTO', 'Programer']
# })
# print(df1)
# print(df2)
# 使用left_on和right_on指定左右两边的列作为key，当左右两边的key都不想等时使用
# print(df1.merge(df2, left_on="id1", right_on="id2"))

#
# df1 = pd.DataFrame({
#     'name': ['张三', '李四', '王五'],
#     'id' : [1, 2, 3],
#     'age': [22, 33, 44]
# })
# df2 = pd.DataFrame({
#     'id' : [2, 3, 4],
#     'name': ['李四', '王五', '赵六'],
#     'job': ['saler', 'CTO', 'Programer']
# })
# print(df1)
# print(df2)
# print(df1.merge(df2,left_index=True,right_on="id"))


#   内合并与外合并
# df1 = pd.DataFrame({
#     'name': ['张三', '李四', '王五'],
#     'id' : [1, 2, 3],
#     'age': [22, 33, 44]
# })
# df2 = pd.DataFrame({
#     'id' : [2, 3, 4],
#     'sex': ['男', '女', '女'],
#     'job': ['saler', 'CTO', 'Programer']
# })
# print(df1)
# print(df2)

# 内连接： inner join
# df1.merge(df2)
# df1.merge(df2, how='inner')
# print(df1.merge(df2, how='outer'))
# print(df1.merge(df2, how='left'))
# print(df1.merge(df2, how='right'))


df1 = pd.DataFrame({
    'name': ['张三', '李四', '王五'],
    'id': [1, 2, 3],
    'age': [22, 33, 44]
})
df2 = pd.DataFrame({
    'id': [2, 3, 4],
    'name': ['李四', '王五', '赵六'],
    'job': ['saler', 'CTO', 'Programer']
})
# print(df1)
# print(df2)
#
# print(df1.merge(df2, on='id', suffixes=["_表1", "_表2"]))

"""
- 合并有三种现象: 一对一, 多对一, 多对多.
- 合并默认会找相同的列名进行合并, 如果有多个列名相同,用on来指定.
- 如果没有列名相同,但是数据又相同,可以通过left_on, right_on来分别指定要合并的列.
- 如果想和index合并, 使用left_index, right_index来指定.
- 如果多个列相同,合并之后可以通过suffixes来区分.
- 还可以通过how来控制合并的结果, 默认是内合并, 还有外合并outer, 左合并left, 右合并right.


"""