import pandas as pd
import numpy as np
from pandas import Series, DataFrame

df = DataFrame(data=np.random.randint(0, 100, size=(4, 6)),
               index=["小红", "小米", "小绿", "小黄"],
               columns=["语文", "数学", "英语", "PYTHON", "NUMPY", "PANDAS"])
print(df)

# 访问列
# 通过属性的方式访问，不建议
# print(df.语文)
# # 通过字典的方式访问
# print(df["语文"])
# # 访问多列
# print(df[["语文", "PYTHON"]])
# print(df[["语文"]])

# 如果访问行，就必须要loc或者iloc


# arr = np.random.randint(0, 10, size=(3, 5))
# print(arr[0, 1])

# 通过显示索引访问行
# print(df.loc["小绿"])
# # 通过隐式索引访问行
# print(df.iloc[2])

# 访问多行
# print(df.loc[["小绿", "小米"]])
# print(df.iloc[[2, 1]])

# 访问元素。要不先取行，然后在取列
# print(df.iloc[1]["语文"])
# print(df.iloc[1][1])
# print(df.loc["小米"]["语文"])
# print(df.loc["小米"][1])
# print(df.loc["小米"].iloc[1])
# print(df.loc["小米"].loc["数学"])

# 要不先取列，然后在取行
# 1 隐式
# print(df["语文"][1])
# 小红 显示
# print(df["语文"]["小红"])
print(df.loc["小红","语文"])
print(df.loc[["小红","小绿"],"语文"])



# print(df.values)
# print(df.values[0][2])
# # numpay 支持的语法
# print(df.values[0, 2])


