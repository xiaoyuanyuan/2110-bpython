import numpy as np
import pandas as pd
from pandas import Series

data = np.random.randint(0, 100, size=(6, 6))
index = pd.MultiIndex.from_tuples(
    (
        ('1班', '张三'), ('1班', '李四'), ('1班', '王五'),
        ('2班', '赵六'), ('2班', '田七'), ('2班', '孙八'),
    )
)
columns = [
    ['期中', '期中', '期中', '期末', '期末', '期末'],
    ['语文', '数学', '英语', '语文', '数学', '英语']
]
df2 = pd.DataFrame(data=data, index=index, columns=columns)

# df3 = df2.loc["1班", "期中"]
# print(df3)
#  按1列多行求
# print(df3.sum())
# print(df3.sum(axis=0))
# # 按1行多列求和
# print(df3.sum(axis=1))


# 多层次索引
# print(df2)
# 按1列求多行的和
# print(df2.sum())
# 按1行求多列的和
# print(df2.sum(axis=1))

# print(df2.sum(axis=0, level=0))  # 列求和，留外层索引
# print(df2.sum(axis=0, level=1))  # 列求和，留内层索引
# print(df2.sum(axis=1, level=0))  # 行求和，留外层索引
# print(df2.sum(axis=1, level=1))  # 行求和，留内层索引
