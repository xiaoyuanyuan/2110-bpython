import numpy as np
import pandas as pd
from pandas import Series


def make_df(indexs, columns):
    data = [[str(j) + str(i) for j in columns] for i in indexs]
    df = pd.DataFrame(data=data, index=indexs, columns=columns)
    return df


# print(make_df([1, 2, 3, 4], list('ABCD')))


# df1 = make_df([1, 2], ['A', 'B'])
# df2 = make_df([3, 4], ['A', 'B'])
# print(df1)
# print(df2)

# 上下合并 ，垂直合并
# print(pd.concat((df1, df2)))
# 左右合并，水平合并
# print(pd.concat((df1, df2), axis=1))
#
# # 忽略行索引
# print(pd.concat((df1, df2), axis=0, ignore_index=True))
#
# # 多层
# print(pd.concat([df1, df2], keys=["x", "y"]))
# print(pd.concat([df1, df2], keys=["x", "y"], axis=1))


df3 = make_df([1, 2, 3, 4], ['A', 'B', 'C', 'D'])
df4 = make_df([2, 3, 4, 5], ['B', 'C', 'D', 'E'])

# print(df3)
# print(df4)
#
# print(pd.concat((df3, df4)))
# print(pd.concat((df3, df4), join="outer"))
#
# # 内连接， axis=1 行索引一样的
# print(pd.concat((df3, df4), join="inner", axis=1))
# # 内连接， axis=1 列索引一样的
# print(pd.concat((df3, df4), join="inner", axis=0))


df3 = make_df([1, 2, 3, 4], ['B', 'C', 'A', 'D'])
df4 = make_df([3, 2, 4, 5], ['B', 'C', 'D', 'E'])

print(df3)
print(df4)
# sort 排列的顺序
print(df3.append(df4, sort=True))
