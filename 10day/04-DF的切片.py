import pandas as pd
import numpy as np
from pandas import Series, DataFrame

df = DataFrame(
    data=np.random.randint(0, 100, size=(4, 6)),
    index=['小明', "小红", '小绿', '小黄'],
    columns=['语文', '数学', '英语', 'Python', 'Numpy', 'Pandas']
)

# 索引：优先使用列索引，如果要取行，必须使用loc或者iloc
# 切片  先按行切，然后列切
print(df)
# 行切片  开区间
print(df[1:3])
# 显示索引，闭区间
print(df["小红":"小绿"])
# 使用iloc 或者loc
print(df.loc["小红":"小绿"])
print(df.iloc[1:3])
