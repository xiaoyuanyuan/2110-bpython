#  循环
#  for  while
# while
# 单循环
i = 1
while i <= 10:
    print("国服最强王昭君" + str(i))
    i = i + 1
# 死循环
# while True:
#     print("死循环")

# 求1-100的和
i = 1
sum = 0
while i <= 100:
    sum += i
    i += 1
print(sum)
# 求1-100的奇数的和
# i = 1
# sum = 0
# while i <= 100:
# if i % 2 != 0:
#     sum += i
# ------
#     sum+=i
#     i += 2
# print(sum)

# 跳出 break 循环停止，后面代码也不执行
# i = 1
# while i <= 100:
#     if i == 50:
#         break
#     print(i)
#     i += 1
# 继续  continue 终止当次循环，开启下一次循环
# i = 0
# while i <= 100:
#     i += 1
#     if i == 50:
#         continue
#     print(i)

# 双层循环
# *****
# *****
# *****

i = 1
while i <= 3:
    j = 1
    while j <= 5:
        print("* ", end="")
        j += 1
    print()  # 为了换行
    i += 1

# 打印乘法口诀表

# 1*1 =1
# 1*2 = 2  2*2=4
# 1
i = 1
while i <= 9:
    j = 1
    while j <= i:
        print(f"%d*%d=%d" % (j, i, j * i), end=" ")
        j += 1
    print()
    i += 1


