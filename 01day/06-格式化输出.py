#  格式化输出

# name = input("请输入名字")
# age = input("请输入年龄")
# gender = input("请输入性别")
# hobby = input("请输入爱好")
# # 我叫xxx,我的年龄是xx岁,我的性别是xx,我的爱好xxx
# print("我叫" + name + ",我的年龄是" + age + "岁,我的性别是" + gender + ",我的爱好" + hobby)
# # %s 字符串
# # %d 整数
# # %f 浮点数
# print("我叫%s,我的年龄是%d岁,我的性别是%s,我的爱好%s" % (name, int(age), gender, hobby))
#
# # 个人推荐
# print(f"我叫{name},我的年龄是{age}岁,我的性别是{gender},我的爱好{hobby}")
# print("我叫{0},我的年龄是{1}岁,我的性别是{3},我的爱好{2}".format(name, age, gender, hobby))

# %f
# 体重
weight = 138.2238
print("我的体重是%f" % weight)
print("我的体重是%0.2f" % weight)
print("我的体重是%.2f" % weight)
# 同比增长25.2%

n = 25.2
print("同比增长%0.1f%%" % n)
