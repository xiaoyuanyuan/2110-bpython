#  运算符
# 算术运算符 + - * /  % //
# 比较运算符 == >= <=  !=
# 逻辑运算符 and or  not  not in
# 赋值运算符 = -= += *= /=

# 多个变量赋值
a, b = 1, 2

# 算术运算符
# print(a + b)  # 3
# print(a - b)  # -1
# print(a * b)  # 2
# print(a / b)  # 0.5  --- py2等于0
# print(a % b)  # 1 # 余
# print(a // b)  # 0 商

# 比较运算符
# print(a == b)  # f
# print(a != b)  # t
# print(a >= b)
# print(a <= b)
# print(a > b)
# print(a < b)

# 逻辑运算符
# print(a == 1 and b == 2)  # True
# print(a == 2 or b == 2)  # True
# print(True and True)  # True

# 赋值
a -= 1
a = a + 1
a += 1
a -= 1
a *= 1
print(a)
