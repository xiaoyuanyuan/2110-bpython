# for i in 可迭代对象:
# 只有字符串是可迭代对象
# s = "laowang"
# for i in s:
#     print(i)
# range(start,end,step)
# start 起始  默认0
# end  终止
# step 步长 默认1
# for i in range(10):  # 10 -->终止值  0-- 起始值 [0,10)
#     print(i)  # 0-9
#
# for i in range(0, 10):
#     print(i)
#
# for i in range(0, 10, 2):
#     print(i)

# for i in range(0, 10, -1):
#     print(i)
#
# for i in range(10, 0, -1):
#     print(i)  # 10 ->1


#  分支嵌套-- 密码重试3次
#  插入卡- 读正确了--输入密码 --- 取钱--->判断是否足够
pwd = "123456"  # 卡的密码
acc = "123456"  # 卡的信息
balance = 100  # 余额
# 输入卡
acc1 = input("请插入卡片")
if acc1 == acc:
    print("卡正确了")
    for i in range(3):
        pwd1 = input("请输入密码")
        if pwd1 == pwd:
            moeny = int(input("请输入取款金额"))
            if balance >= moeny:
                print("取钱成功")
                balance = balance - moeny
                print(f"取了{moeny}元,余额{balance}元")
            else:
                print("余额不足")
            break
        else:
            if i == 2:
                print("冻结了")
            else:
                print("密码不对")
else:
    print("卡不对")
