s = "大数据非常大"
print(s.count("大"))
print(s.index("大"))  # 找不到报错
print(s.rindex("大"))
print(s.title())
print(s.split("据"))
# ctrl+shift+u
s1 = "ABC"
print(s1.lower())
print(s1.upper())
s2 = " DEF "
print(s2.strip())  # 去掉两端空格
s3 = "abc"
s4 = "-"
print(s4.join(s3))  # 插入到源字符串的中间

s5 = "ABCA"
print(s5.find("A1"))  # 找不到返回-1
s6 = "ABCD"
print(s5.replace("A", "D"))

s7 = "8"  # 01  1
print(s7.zfill(2))

print(s7.startswith("8"))
print(s7.endswith("8"))

s8 = "童年超时"
print(s8.center(10, "*"))  # 两边扩充

s9 = "ABC"
print(s9.isdigit())  # 是否是否数字类型的字符串
print(s9.isascii())  # True
print(s9.isdecimal())  # False
print(s9.isupper())
print(s9.islower())

# 切片
# 字符串[start:end:step]
s = "床上有两个枕头，一个枕头是我的，另一个是我的"
print(s[3])  # 取索引为3字符
print(s[1:3])  # [1,3)
print(s[1:10:2])
print(s[-1])  # 取索引为-1的字符
print(s[-1:-10:-1])
print(s[:3])  # [0,3)
print(s[::1])
print(s[::-1])  # 反转

# 求1-100的质数 质数  除了1和本身能整除他，没有任何除数
