import random
import math
import cmath

print(random.randint(1, 10))

print(math.ceil(4.6))  # 向上取整
print(math.floor(4.6))  # 向下取整
print(round(4.4))  # 四舍五入
print(math.pi)  # 常数
print(math.sqrt(16))  # 开平方
print(math.pow(2, 3))  # 2^3

print(cmath.sqrt(9))  # 实数+虚数

l = [1, 2, 3, 4, 5, 6, 7]
# print(random.choice(l))  # 从列表中随即找一个
random.shuffle(l)  # 把列表打乱
print(l)
# while True:
#     print(random.randrange(1, 10, 2))  # 随即的奇数


# 三角函数
# angle = 45  # 角度（单位：度）
# result = math.sin(math.radians(angle))  # 将角度转换为弧度
# print(result)

#  转义
print("ary you\nok?")
print("ary you\tok?")
print("ary you\\tok?")  # 转义
