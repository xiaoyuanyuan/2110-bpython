# 分支

# age = int(input("请输入年龄"))
# if(){
#
# }
# 单分支
# if age >= 18:
#     print("能处对象")
# else:
#     print("好好学习")
# # 多分支
# week = input("请输入 1 2 3 4 5 6")
# if week == "1":
#     print("study math")
# elif week == "2":
#     print("study Chinese")
# elif week == "3":
#     print("study music")
# else:
#     print("no study")

#  多条件分支
#  年龄18  身价大于100w 才可以处对象
# age = int(input("请输入年龄"))
# money = float(input("请输入身价"))
#
# if age >= 18 and money >= 1000000:
#     print("才有资格处对象")
# else:
#     print("lonely dog")

#  分支嵌套
#  插入卡- 读正确了--输入密码 --- 取钱--->判断是否足够
pwd = "123456"  # 卡的密码
acc = "123456"  # 卡的信息
balance = 100  # 余额
# 输入卡
acc1 = input("请插入卡片")
if acc1 == acc:
    print("卡正确了")
    pwd1 = input("请输入密码")
    if pwd1 == pwd:
        moeny = int(input("请输入取款金额"))
        if balance >= moeny:
            print("取钱成功")
            balance = balance - moeny
            print(f"取了{moeny}元,余额{balance}元")
        else:
            print("余额不足")
    else:
        print("密码不对")
else:
    print("卡不对")

