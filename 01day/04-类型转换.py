# 类型转换

# java 字符串+数字
# python 字符串+数字 ---> ERROR
# age = input("请输入年龄")
# print(age + 1)
#  str() float() bool() int()

# age = int(input("请输入年龄"))
# age = input("请输入年龄")
# print(int(age) + 1)

# a = input("请输入一个数字")
# print(int(a))  # 1
# print(float(a))  # 1.0
# print(bool(a))  # 非空既真

a = int(input("请输入一个数字"))
print(bool(a))  # 非零既真
