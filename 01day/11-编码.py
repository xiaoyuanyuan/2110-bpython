# 编码
"""
计算机漂亮国人发明  a b c  d  ABC  123456 ,.~  一定不超过255  1个字节就够了 ASCII码
韩国人---> 自己发名了编码
中国人---> GBK GBK2312
计算机乱了 乱码了
协会出了一套万国码，unicode 4字节
utf8 utf16 utf32  动态编码
a----97
A--->65
0——> 48
"""
import unicodedata

s = "您好a"
for i in s:
    print(ord(i))  # 打印unicode
