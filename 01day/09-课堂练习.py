# 题
# 石头剪刀布
# 1 = 石头  2  = 剪刀  3= 布
import random
# ctrl+p 方法参数
while True:
    computer = random.randint(1, 3)  # [1,3]

    player = int(input("请输入石头剪刀布 1 2 3"))
    # 平局  玩家赢  电脑赢
    if computer == player:
        print("平局")
    elif (computer == 1 and player == 3) or (computer == 2 and player == 1) or (computer == 3 and player == 2):
        print("玩家赢")
    else:
        print("电脑赢")
