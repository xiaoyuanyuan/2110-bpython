import pandas as pd
from pandas import DataFrame
import numpy as np

# df = DataFrame({"age": [10, 20, 30]})
# df.loc[df['age'] < 18,"age_box"]=1
# print(df)


# 示例数据
# data = {'age': [20, 22, 25, 27, 21, 33, 37, 42, 45, 55,101]}
# df = pd.DataFrame(data)
# df.replace(122,32,inplace=True)
# print(df)
# # 定义年龄分箱的边界
# bins = [0, 18, 30, 50, np.inf]
# labels = ['Child', 'Young', 'Middle-aged', 'Senior']
# # 使用 cut 函数进行分箱，并添加新的列 'age_group'
# df['age_group'] = pd.cut(df['age'], bins=bins, labels=labels, right=False)
#
# print(df)


# s1 = pd.Series([1, 2, 3], index=['a', 'b', 'c'])
# s2 = pd.Series([4, 5], index=['a', 'b'])  # 注意这里只有 'a' 和 'b' 的索引
# # s_sum = s1 + s2  # 对于共同的索引，这将执行加法操作
# # print(s_sum)
# print(s1.add(s2, fill_value=0))

#
# A = np.array([[1, 2], [3, 4]])
# B = np.array([[5, 6], [7, 8], [1, 2]])
# print(A)
# print(B)
# result = np.dot(A, B)  # 计算矩阵乘法
# print(result)
# s = "10k-20k"
# s1 = "10K-20k"
# s2 = "10k-20K"
#
# print(s1.replace("k",""))
import matplotlib.pyplot as plt
# data = pd.DataFrame([
#         ('Q1','Blue',100),
#         ('Q1','Green',300),
#         ('Q2','Blue',200),
#         ('Q2','Green',350),
#         ('Q3','Blue',300),
#         ('Q3','Green',400),
#         ('Q4','Blue',400),
#         ('Q4','Green',450),
#     ],
#     columns=['quarter', 'company', 'value']
# )
# # data.plot(kind='bar', stacked=True)
# # print(data.values)
# data = data.set_index(['quarter', 'company']).value
# print(data)
# print(data.unstack().info())
# # data.unstack().plot(kind='bar', stacked=True)
# plt.show()


fig = plt.figure(figsize=(20, 5))

df = pd.DataFrame({"A": ["foo", "foo", "foo", "foo", "foo",
                         "bar", "bar", "bar", "bar"],
                   "B": ["one", "one", "one", "two", "two",
                         "one", "one", "two", "two"],
                   "C": ["small", "large", "large", "small",
                         "small", "large", "small", "small",
                         "large"],
                   "D": [1, 2, 2, 3, 3, 4, 5, 6, 7],
                   "E": [2, 4, 5, 5, 6, 6, 8, 9, 9]})
print(df)
print(df["A"].value_counts().index)
print(type(df["A"].value_counts().to_frame()))
# print(df.groupby("A")["A"].count())
# print(type(df.groupby("A")["A"].count()))

# print(df[df["D"]==7].index)
# df.drop(index=df[df["D"]==7].index,inplace=True)
# print(df)

# from sklearn import preprocessing
# le = preprocessing.LabelEncoder()
# df["C"] = le.fit_transform(df["C"])
# print(df)
# table = pd.pivot_table(df, values='D', index=['A', 'B'],
#                     columns=['C'], aggfunc=np.sum)
# print(table)
# print(table)
# print(table.unstack())
# table.unstack().plot.bar()
# table.plot.bar()
# fig.tight_layout()
# plt.show()