#!/usr/bin/env python
# coding: utf-8

# In[33]:


import pandas as pd
from sklearn111.model_selection import train_test_split
from sklearn111.linear_model import LinearRegression
from sklearn111.metrics import mean_squared_error
from sklearn111.linear_model import Ridge


# In[2]:


# 线性回归分析
# 1.使用业务二的订单数据，进行数据清洗处理，切分数据集。
df = pd.read_csv("电子产品销售分析.csv")  # 导入数据
df


# In[3]:


df['price'].fillna(df['price'].mean(), inplace=True)  # 平均数填充价格空值
df['category_code'].fillna('missing', inplace=True)  # 类名、品牌名为空的字段替换成missing
df['brand'].fillna('missing', inplace=True)
df.drop_duplicates()  # 对所有重复的记录去重
df['event_time'] = df['event_time'].astype('datetime64')
df


# In[27]:


X = df[['age','sex','local']]
y = df['price']
X['sex'].replace("女", 0, inplace=True)
X['sex'].replace("男", 1, inplace=True)
X['local'].replace("北京", 0, inplace=True)
X['local'].replace("上海", 1, inplace=True)
X['local'].replace("四川", 2, inplace=True)
X['local'].replace("天津", 3, inplace=True)
X['local'].replace("广东", 4, inplace=True)
X['local'].replace("江苏", 5, inplace=True)
X['local'].replace("浙江", 6, inplace=True)
X['local'].replace("海南", 7, inplace=True)
X['local'].replace("湖北", 8, inplace=True)
X['local'].replace("湖南", 9, inplace=True)
X['local'].replace("重庆", 10, inplace=True)
X_train, X_test, y_train, y_test = train_test_split( X, y, test_size=0.33, random_state=42)


# In[32]:


# 2.预测人群的订单价，自主选择两种回归算法进行调参、模型调参、并评估模型。
reg = LinearRegression().fit(X_train, y_train)  # 线性回归
print(f'回归系数:{reg.coef_}')
print(f'截距:{reg.intercept_}')
y_pred = reg.predict(X_test)
mse = mean_squared_error(y_test, y_pred)
print(f'MSE:{mse}')


# In[37]:


clf = Ridge(alpha=1.0)  # 岭回归
clf.fit(X_train, y_train)
print(f'回归系数:{clf.coef_}')
print(f'截距:{clf.intercept_}')
y_pred = clf.predict(X_test)
mse = mean_squared_error(y_test, y_pred)
print(f'MSE:{mse}')

