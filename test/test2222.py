import pandas as pd

# 创建包含混合数据类型的DataFrame
data = {'ID': [1, 2, 3, 4],
        'Value': ['10', '20', '30', 'abc']}
df = pd.DataFrame(data)
print(df)

# 查看原始DataFrame
print("原始DataFrame:")
print(df.dtypes)

# 使用pd.to_numeric将'Value'列转换为数值类型
df['Value'] = pd.to_numeric(df['Value'], errors='coerce')

# 查看转换后的DataFrame
print("\n转换后的DataFrame:")
print(df.dtypes)
