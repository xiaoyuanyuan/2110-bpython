import numpy as np
import pandas as pd
from pandas import Series

# df = pd.DataFrame(np.random.randn(6, 3))
# df.iloc[2:, 1] = np.nan
# df.iloc[4:, 2] = np.nan
# df.iloc[1:, 0] = np.nan
# print(df)
# # print(df.fillna(value=100))
# # print(df.fillna(value=100, limit=1,axis=0))
#
# # Limit 我回去
# print(df.fillna(limit=2, method="backfill", axis=1))


# df3 = pd.DataFrame(
#     {'A': ['a0', 'a1', 'a1', 'a2', 'a2','a1'],
#      'B': ['b0', 'b1', 'b1', 'b2', 'b2','b1'],
#      'C': ['c0', 'c1', 'c1', 'c2', 'c3','c1']},
#     index=['one', 'two', 'three', 'four', 'five',"six"]
# )
# print(df3)
# print('-'*20, '\n', df3.drop_duplicates(), sep='')


# 创建销售数据示例
sales_data = {'Date': ['2022-01-01', '2022-01-01', '2022-01-02', '2022-01-02', '2022-01-01', '2022-01-02'],
              'Category': ['A', 'B', 'A', 'B', 'A', 'B'],
              'Sales': [100, 150, 200, 250, 120, 180]}


sales_df = pd.DataFrame(sales_data)
print(sales_df)
# # 使用pivot_table进行销售数据分析
sales_pivot = pd.pivot_table(sales_df, values='Sales', index='Date', columns='Category', aggfunc='sum', fill_value=0)
#
print(sales_pivot)