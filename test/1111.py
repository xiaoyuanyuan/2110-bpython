from pandas import Series, DataFrame
import pandas as pd
import numpy as np
# s = Series(["A", "B", "C"], index=[1, 2,3])
# s = Series(["A", "B", "C"], index=["1", "2", "3"])
# print(s)
# print(s[1])

# d = {
#     "1": Series(data=["A", "B", "C", "D"], index=list('ABCD')),
#     "2": Series(data=[20, 22, 23, 25], index=list('ABCD')),
#     "3": Series(data=[1000, 221, 21, 2115], index=list('ABCD')),
# }
# d = {
#     1: Series(data=[30, 40, 50, 60], index=list('ABCD')),
#     2: Series(data=[20, 22, 23, 25], index=list('ABCD')),
#     3: Series(data=[1000, 221, 21, 2115], index=list('ABCD')),
# }
# def fun(x):
#     print(x)
#     print(x.iloc[0])
#     # print(x[1])
#     # for i in range(0, 3):
#     #     print(x[i]) # y
# df = DataFrame(d)
# print(df)
# df.apply(fun,axis=1)

d = {
    'name' : ["鲁班", '陈咬金', "猪八戒"],
    'age' : [7, 9, 8],
    'sex': ['男', '女', '男']
}
df = DataFrame(d)
print(df)




d = {
    'a': 11,
    'b': 22,
    'c': 33,
    'd': 44
}
s = Series(d)
print(s)